# Fleischwurst-Kohlrabi-Eintopf

**4 Portionen**

---

- *400 g* Fleischwurst
- *4* Kohlrabi
- *300 g* Erbsen, TK
- *2 TL* Speisestärke
- *200 g* Sahne
- *500 ml* Wasser
- *2 TL* Gemüsebrühepulver
- Pfeffer
- optional: Salzkartoffeln als Beilage

---

Kohlrabi schälen und in Stifte schneiden. Die Fleischwurst in nicht zu kleine Würfel schneiden.

500 ml Wasser mit Gemüsebrühe in einen großen Topf geben. Die Kohlrabistifte zugeben und ca. 5 Min. kochen. Dann die Fleischwurst zufügen und weitere 5 Min. köcheln. Die Erbsen einrühren und erneut weitere 5 Min. leicht kochen lassen. Dann die Speisestärke mit der Sahne verrühren, in den Eintopf geben und kurz unter Rühren aufkochen lassen. Zum Schluß mit Pfeffer abschmecken.

Dazu Salzkartoffeln reichen.

---

Quelle: https://www.chefkoch.de/rezepte/569241155575777/Fleischwurst-Kohlrabi-Eintopf.html