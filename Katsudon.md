# Katsudon

Japanisches Schweineschnitzel mit Ei

**3 [Lerngruppz](README.md#lerngruppz)**

---

- *500 g* Schweineschnitzel
- *8* Eier
- *4* Zwiebeln
- *250 g* Reis
- *200 ml* Hühnerbrühe
- *2 EL* Sojasoße
- *2 TL* Zucker
- Salz
- Mehl
- Paniermehl
- Öl zum Braten
- Schnittlauch

---

Eine Pfanne mit Öl heiß machen.  
Das Fleisch mit Mehl, einem Viertel der Eier und Paniermehl panieren.  
Das Ei wird später noch gebraucht!  
Von beiden Seiten kurz braten.  
In Streifen schneiden.  
Beiseite stellen.

Reis nach Packungsanleitung kochen.

Zwiebeln in Halbringe schneiden.  
In der Pfanne anschwitzen. 

Aus Hühnerbrühe, Sojasoße und Zucker eine Soße mischen.  
Die Zwiebeln damit ablöschen.  
Die Schnitzel auf die Zwiebeln legen.

Das übriggebliebene Ei vom Panieren mit den restlichen Eiern grob verquirlen und über den Zwiebeln verteilen.  
Mit geschlossenem Deckel braten, bis das Ei gestockt ist.

Mit Schnittlauch garnieren.