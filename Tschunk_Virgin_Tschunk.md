# Tschunk, Virgin Tschunk

Angelehnt an das schicke alkoholische Nationalgetränk aus Brasilien, dem Caipirinha, braut sich der Hacker unter Hinzugabe von Mate seinen eigenen Cocktail.

*Cocktail*, *alkoholisch*, *alkoholfrei*

**600 ml**

---

- *1/2* Limette
- *20 g* brauner Zucker
- *4* Eiswürfel
- *4 cl* Havana Club
- *200 ml* Mate nach Wahl (Club Mate, Flora Mate, Mio Mio Mate, etc)
- Strohhalm

---

Limette mit Zucker stößeln, Eiswürfel und Rum dazugeben, mit Mate aufgießen.  
Mit Strohhalm servieren.  
Virgin Tschunk: Ohne Rum.

## Siehe auch
- https://entropia.de/Tschunk
- https://chre.kocht.gulasch.myblog.de/chre.kocht.gulasch/art/3746170

