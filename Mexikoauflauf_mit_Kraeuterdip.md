# Mexikoauflauf mit Kräuterdip

Wenn Chips über sind und man Lust auf Hackfleisch und Käse hat.
Originalrezept verwendet Veggiehack.

WENN Rindfleisch dann helal.  
WENN Veggiehack, dann vegetarisch (nicht vegan weil Käse).

![](img/chips-mexiko-auflauf.jpg)

*mexican*

**3 [Lerngruppz](README.md#lerngruppz)**

--- 

## Auflauf

- *2 EL* Tomatenmark
- Öl
- *1* Zwiebeln
- *750 g* Hackfleisch (ggf. Veggiehack)
- *3 EL* Sojasauce
- *1* Zehe Knoblauch
- *1* Schote Chili
- *150 g* Mais
- *200 g* Kidneybohnen
- *1* Paprika (Farbe egal)
- *0,50 TL* Kreuzkümmel ("Cumin")
- *200 g* Pizzatomaten
- Salz
- Pfeffer
- *150 g* geriebener Cheddar
- *250 g* (Tortilla) Chips

## Dip

- *400 g* saure Sahne
- *3 EL* Schnittlauch (gehackt)
- *3 EL* Koriander
- *1 EL* Minze
- *2* Limetten, Saft

---

### Schritt 1, Grundmasse

Ofen auf 190 °C vorheizen.  
Tomatenmark in etwas Öl kurz anbraten.  
Zwiebeln schälen, würfeln und mit dem (Veggie-)Hack hinzufügen,  
ca. 3–4 Min. unter Rühren scharf anbraten.

### Schritt 2, Grundmasse

Sojasauce, Knoblauch, Chili, Mais, Kidneybohnen, Paprika, Cumin und Pizzatomaten dazugeben,  
kurz aufkochen,  
mit Salz und Pfeffer abschmecken.

### Schritt 3, in Form füllen

Mit der Gemüsemasse einen Kranz am Rand der Auflaufform bilden,  
mit Käse bestreuen,  
(Tortilla-)Chips kreisförmig anschmiegen,  
dann wieder Gemüse, Käse, Tortilla Chips usw. hineingeben,  
bis die Form gefüllt ist. I 
m Ofen 10 min. backen.

### Schritt 4, Dip dazu machen

Für den Dip alle Zutaten miteinander verrühren und zum Auflauf servieren.  
Der Dip schmeckt zusammen mit dem Auflauf wirklich super-lecker und gibt dem Rezept das gewisse etwas.

---

Quelle: [REWE Superbowl Werbung](https://www.rewe.de/rezepte/mexikoauflauf-kraeuterdip/)
