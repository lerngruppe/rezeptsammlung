# Bandnudeln mit frischem Spinat und Lachs

Leckeres Gericht mit Spinat

*quick, fish, halal*

**3 servings**

---

- *600 g* Bandnudeln, frische (optional 200 - 250 g Trockenware)
- *500 g* Blattspinat, frischer
- *1* Zwiebel
- *2* Zehe/n Knoblauch
- *1 TL* Gemüsebrühe, (instant)
- *125 ml* Wasser
- *1 Pkt.* Lachs, geräucherter, in Scheiben (oder auch frischer Lachs)
- *1 Becher* Cremefine oder Schmand
- *1 TL* Speisestärke
- *2 EL* Rapsöl oder anderes raffiniertes Öl
- Salz
- Pfeffer
- Muskat 

---

Die Nudeln nach Gebrauchsanweisung kochen - Achtung, frische Nudeln brauchen nur 2 - 3 Minuten (also aufs Timing achten)!

Den Spinat von Stängeln (bis zum Blattanfang) befreien und gründlich waschen. Sand bekommt man am besten raus, indem man den Spinat in Wasser legt und nicht nur abbraust.

Die Zwiebel in Ringe schneiden, in eine Pfanne mit hohem Rand und Deckel mit dem Öl geben und bei kleiner bis mittlerer Hitze glasig dünsten (nicht braten!). Gemüsebrühe mit Wasser mischen und dazu gießen (alternativ geht auch Weißwein statt Brühe). Den Knoblauch schälen, in möglichst kleine Stückchen schneiden und in die Pfanne geben. Nun den Spinat dazugeben. Evtl. geht das nur nach und nach, er fällt aber schnell in sich zusammen, so dass nachgelegt werden kann, falls die Pfanne nicht groß genug ist. Den Räucherlachs in Stücke schneiden und dazugeben, sobald der Spinat komplett in sich zusammen gefallen ist. Alternativ zum Räucherlachs geht auch frischer Lachs, der auf die gleiche Weise einfach gewürfelt und noch roh dazugegeben werden kann.

Etwas Flüssigkeit abnehmen und in einer Tasse mit der Stärke mischen, bis sie sich löst. Dieses Gemisch wieder in die Pfanne geben, ebenso den Becher Cremefine bzw. Schmand.

Mit Pfeffer, Salz und (am besten frisch hinein geriebener) Muskatnuss würzen, die Nudel abgießen und untermischen und servieren. 

Quelle: https://www.chefkoch.de/rezepte/1677441275813669/Bandnudeln-mit-frischem-Spinat-und-Lachs.html
