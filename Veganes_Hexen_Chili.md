# Veganes Hexen-Chili

Chili ohne Fleisch und mit Malzbier statt dunklem Bier.
Ursprünglich kennengelernt auf Parties im Umfeld des Stratum 0 e. V.

[Variante mit Bier und Kaffee](https://www.chefkoch.de/rezepte/2272481362691762/Veganes-Coffee-Kakao-Bier-Chili.html)

*vegan, vegetarian, halāl*

**6 Portionen**

---

- *250 g* Sojagranulat, grob
- *10 EL* Sojasauce
- *5 EL* Gemüsebrühe
- *3* Zwiebel(n)
- *3* Knoblauchzehe(n)
- *1 Pck.* Tomaten, passierte
- *70 g* Tomatenmark
- *1 Glas* Tomate(n), getrocknete, in Öl eingelegt
- *250 ml* Malzbier
- *2 EL* Zucker, braun
- *1 1/2 EL* Kakaopulver
- *1/2 TL* Oregano
- *1/2 TL* Majoran
- *3* Chilischote(n)
- *2 Dose(n)* Kidneybohnen, abgegossen
- *1 Dose(n)* Mais, abgegossen
- Pfeffer
- Cayennepfeffer
- Muskat
- Paprikapulver
- etwas Öl

---

### Schritt 0 - Input Soja
- Die Sojastücke in eine große Schüssel geben und mit der Sojasoße und der Gemüsebrühe gut vermischen.
- Dann mit kochendem Wasser übergießen. Die Schüssel sollte mindestens 3/4 voll sein, damit die Sojastücke ordentlich aufgehen können.
- Ziehen lassen, bis die Sojastücke etwa die Konsistenz von angebratenem Hackfleisch haben.

### Schritt 1 - Input Geschmachsträger
- Zwiebel würfeln, Knoblauch zerkleinern, getrocknete Tomaten in kleine Streifen schneiden, Chilischoten sehr klein schneiden. 
- Zwiebeln und Knoblauch in ein wenig Öl andünsten, bis die Zwiebeln glasig werden.
- Chilischoten hinzugeben und kurz mitdünsten.
- Passierte Tomate, Tomatenmark, Malzbier, Kakaopulver, Kräuter hinzufügen und kurz aufkochen.

### Schritt 2 - delete Feuchtigkeit
- Die Flüssigkeit von den Sojastücken abgießen. Je weniger man salzig mag, desto besser sollte die Sojasoße abgegossen werden.
- Evtl. die Sojastücke ein wenig ausdrücken.

### Schritt 3 - Input mehr Geschmachsträger
- Sojastücke, Bohnen, getrocknete Tomate und Mais in die Tomatensoße geben. Wenn das Chili jetzt zu dick ist, Wasser hinzugeben, bis das Mischungsverhältnis stimmt.
- Mit weiteren Gewürzen wie z.B. Cayennepfeffer, Paprika, Muskat, Pfeffer, abschmecken. Salzig genug sollte das Chili durch Sojasoße und Gemüsebrühe sein.

### Schritt 4 - Kochen
- Das Chili jetzt ca eine halbe Stunde auf niedriger Stufe köcheln lassen. Je länger es danach zieht, desto leckerer wird es, aber man kann es auch direkt essen.

### Beilagen
Dazu passen Reis, Nudeln, Fladenbrot, Baguette.

Übernommen von [tstehr/recipes](https://github.com/tstehr/recipes/blob/master/Veganes_Coffee_Kakao_Bier_Chili.md), ursprüngliche Quelle: [usa-kulinarisch.de](https://www.usa-kulinarisch.de/rezept/coffee-kakao-bier-chili-vegane-version/)
