# Französischer Kartoffeltopf

**1 Unbekanntemenge**

---

- *4* Koteletts (andere Fleischarten sind auch möglich. Ggf. stärker / anders würzen.)
- *40 g* Margarine
- *200 g* Zwiebeln
- *750 g* Kartoffeln
- *1* Knoblauchzehe
- Salz
- Pfeffer
- Paprika
- gehackte Petersilie

---

- Kotletts klopfen, in heißer Margarine kurz anbraten
- Zwiebel- u. Kartoffelscheiben drauf schichten
- zerdrückte Knoblauchzehe dazu
- mit kochendem Wasser oder Gemüsebrühe bis zur Hälfte auffüllen, würzen, zugedeckt dünsten
- mit Petersilie bestreuen
- (Zubereitung im Backofen möglich, dann mehrmals übergießen)

---

Quelle: Familien Kochbuch von Karl