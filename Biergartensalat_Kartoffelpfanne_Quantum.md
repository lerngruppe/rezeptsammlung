# Biergartensalat-Kartoffelpfanne-Quantum

Kalt kann dieses Gericht als Biergartensalat bezeichnet werden, warm auch als Kartoffelpfanne.

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1 kg* Kartoffeln, festkochend oder vorwiegend festkochend
- *1* Porree
- *500 g* Radieschen
- *300 g* Gewürzgurken süß-sauer
- *1/2 Bund* Schnittlauch
- *125ml* Gemüsebrühe
- *4 EL* Essig
- *4 EL* Gurkenwasser
- *4* Zwiebeln
- *400 g* Leberkäse
- Salz
- Pfeffer
- Senf
- Zucker
- Mehl

---

Kartoffeln schälen.  
In Salzwasser aldente kochen.  
Abgießen und in ca 5mm dicke Scheiben schneiden.

Porree waschen und in Ringe schneiden.  
Porree in Gemüsebrühe und Essig einige Minuten kochen.  
Mit Salz, Pfeffer, Senf und Zucker abschmecken.  
Über die Kartoffeln gießen und ca. 30 Minuten ziehen lassen.

Radieschen und Schnittlauch waschen.  
Radieschen, Schnittlauch und Gewürzgurken klein schneiden.  
Mit den Kartoffeln vermengen.

Die Zwiebeln in Ringe schneiden.  
In Mehl wenden.  
In viel Öl goldbraun braten.

Den Leberkäse von beiden Seiten braten.

Alles zusammen vermengen.
