# Erdbeereis

Ohne Eismaschine.

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *150 g* Frischkäse
- *300 g* Tiefkühlerdbeeren
- *2 TL* Zucker

---

Alle Zutaten in einem Mixer mischen.  
Im Gefrierschrank 2 Stunden einfrieren, dabei alle 20 Minuten gut durchrühren.
