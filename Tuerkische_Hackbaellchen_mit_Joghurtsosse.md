# Türkische Hackbällchen mit Joghurtsoße

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1* Zwiebel
- *3 Zehen* Knoblauch
- *2 EL* gehackte Minze
- *1* Ei
- *40 g* Semmelbrösel
- *1 EL* Tomatenmark
- *500 g* Lammhackfleisch, ersatzweise Rinderhack
- *300 g* [Joghurt-Minz-Soße](Joghurt-Minz-Soße.md)
- Öl zum Braten.

---

Zwiebel und Knoblauch sehr fein würfeln.  
Beides mit Minze, Ei, Semmelbrösel und Tomatenmark unter das Hackfleisch kneten.  
Öl in einer Pfanne erhitzen.  
Kleine Bällchen formen.  
5 bis 8 Minuten braten.

---

Dazu passt türkisches Fladenbrot.

Quelle: https://www.chefkoch.de/rezepte/1178021223968697/Tuerkische-Hackbaellchen-mit-Joghurtsauce.html
