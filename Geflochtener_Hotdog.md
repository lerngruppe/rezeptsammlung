# Geflochtener Hotdog

*einfach*

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *1 Rolle* Blätterteig aus dem Kühlregal (ca. 25x38 cm)
- *8 Würstchen*, nach Belieben Hotdog oder Wiener
- *2 EL* Senf, mittelscharfer oder scharfer
- *4 EL* Ketchup
- *4 EL* Mayonnaise oder Hotdogsauce
- *150 g* Käse, Scheiben oder geraspelt
- *250 g* Gewürzgurken
- *1* Ei
- einige Röstzwiebeln nach Belieben

---

Die Gewürzgurken längs in Streifen schneiden.  
Den Blätterteig ausbreiten.  
Die Würstchen darauf legen, 4 nebeneinander und 2 hintereinander. Sollte etwa so aussehen:
```
┌────────┐
│        │
│  ║║║║  │
│  ║║║║  │
│  ║║║║  │
│  ║║║║  │
│  ║║║║  │
│  ║║║║  │
│        │
└────────┘
```
Die Würstchen dick mit dem Senf, Ketchup und Mayo bestrichen.  
Die Gewürzgurken darauf verteilen.  
Den Käse darauf legen.  
Darauf dann eine flasche Schicht Röstzwiebeln.  

Den überstehenden Blätterteig an beiden Seiten schräg in 2cm breite Streifen schneiden.  
Diese im Wechsel über die Füllung falten, damit es geflochten aussieht.  
Die Enden zuklappen und leicht andrücken.  

Den Ofen auf 190°C vorheizen (Umluft).  

Das Ei mixen.  
Damit den Teig gründlich bestreichen. 
Nach belieben mehr Röstzwiebeln draufstreuen.

20 Minuten backen.

---

Quelle: https://www.chefkoch.de/rezepte/3346681497361120/Geflochtener-Hotdog.html
