# Wirsing mit Kokos und Couscous

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *4* Zwiebeln
- *1 Kopf* Wirsing
- *2 Zehen* Knoblauch
- *800 ml* Kokosmilch
- *500 g* Couscous
- *4 TL* Kurkuma
- *100 ml* Sahne
- Butterschmalz
- etwas Zucker
- Salz
- Pfeffer

---

Die Zwiebeln schälen und grob hacken.  
In einem großen Topf mit etwas Butterschmalz anbraten.

Wirsing grob zerkleinern.  
Waschen.  
Nach und nach zu den Zwiebeln geben und leicht anbraten.  
Immer wieder umrühren und evtl etwas Wasser angießen damit nichts anbrennt.  
Kochen lassen, bis die gewünschte Bissfestigkeit fast erreicht ist.

In einem kleinen Topf etwas Butterschmalz erhitzen.  
Knoblauch klein hacken.
Kurkuma und gehackten Knoblauch mit etwas Zucker leicht anbraten, ohne dass die Gewürze bräunen.
Mit 3/4 der Kokosmilch ablöschen.  
Couscous einstreuen, kurz aufkochen. (Achtung! Spritzt gerne und die Flecken von Kurkuma gehen meist nur mit der Fleckenschere raus!)  
Die Sahne hinzugeben.  
Den Topf von der Platte nehmen und Couscous gar ziehen lassen.  

Den Wirsing gegen Ende der Garzeit mit dem letzten 1/4 der Kokosmilch statt mit Wasser begießen und salzen.  
Wenn der Wirsing die gewünschte Bissfestigkeit erreicht hat und das Couscous auf der Zunge zergeht, ist alles so wie es sein soll.

Mit Pfeffer servieren.

---

Quelle: https://www.chefkoch.de/rezepte/1279041233246617/Wirsing-mit-Kokos-und-Couscous.html
