# Erdnuss-Tofu-Salat

30 min

*calories_under_500, asian_fusion, diet_vegan, diet_dairy_free, main_ingredient_salad, main_ingredient_vegetables, diet_vegetarian, budget_above_15_us, main_dish, budget_above_15_de, baked, dinner_lunch, main_ingredient_pasta, calories_under_600*

**2 Portionen**

---

- *100 g* Glasnudel
- *1* Karotte
- *2* Frühlingszwiebel
- *1* rote Paprika
- *1/4* Rotkohl
- *100 g* Tofu, natur
- *2 EL* Sojasauce
- *2 Zehen* Knoblauch
- *0.5 EL* Chiliflocke
- *1 TL* Speisestärke
- *2 EL* Agavendicksaft
- *2* Limetten
- Salz
- *30 g* Erdnussbutter
- *15 g* Koriander, frisch
- *15 g* Minze, frisch
- *80 g* Blattspinat, frisch

---
 
Backofen auf 200°C (Ober- und Unterhitze, empfohlen) oder 180°C (Umluft) vorheizen.

Limette gründlich mit heißem Wasser waschen, Schale abreiben, halbieren und den Saft auspressen.

Knoblauch schälen und reiben.

Tofu zwischen mehrere Schichten Küchenpapier legen und möglichst viel Flüssigkeit herausdrücken, anschließend in Scheiben schneiden oder würfeln.

In einer Schüssel Erdnussbutter, Knoblauch, Agavendicksaft, Chiliflocken, Limettensaft und -abrieb, Stärke, Sojasauce und etwas Wasser mischen.

Tofu in Marinade wenden.

Tofu auf ein mit Backpapier belegtes Backblech legen und 20 min. backen.

Restliche Marinade zur Seite stellen. Mit Salz würzen.

Reisnudeln nach Packungsanleitung zubereiten.

Karotten waschen, schälen und mit dem Gemüseschäler in dünne Streifen schneiden.

Rotkohl waschen und in feine Streifen schneiden.

Spinat waschen und abtropfen lassen.

Währenddessen Paprika waschen, entkernen und in dünne Streifen schneiden.

Minze waschen, trocken schütteln und Blätter abzupfen.

Koriander waschen, trocken schütteln und Blätter abzupfen.

Frühlingszwiebeln waschen und in feine Ringe schneiden.

Gemüse mit Dressing mischen, mit Tofu anrichten und nach Wunsch mit mehr Kräutern und Limette garnieren.

---

Quelle: Martina (https://blog.kptncook.com/2020/11/05/durfen-wir-vorstellen-unsere-koch-kptns/) via https://sharing.kptncook.com/SWOf1UiXVnb
