# Schnelle Thai Suppe

Nicht authentisch thailändisch, aber lecker.

**6 [Lerngruppz](README.md#lerngruppz)**

---

- *4* Süßkartoffeln
- *2* Auberginen
- *2* Zucchini
- *4 EL* Sesamöl
- *100 g* rote Currypaste
- *2 Liter* Gemüsebrühe
- *4 EL* Kokosrapseln
- *400 bis 500 g* Glasnudeln
- *40 g* Koriander
- *1* Limette, Saft
- etwas Sojasoße zum Abschmecken

---

Süßkartoffeln schälen.  
Auberginen und Zucchini waschen.  
Alles in Würfel schneiden.

Sesamöl in einem großen Topf erhitzen.  
Rote Currypaste kurz anrösten.  
Mit Gemüsebrühe und Kokosmilch aufgießen.
Gemüse und Kokosraspeln dazugeben.
Circa 20 Minuten köcheln lassen.

In einem anderen Topf die Glasnudeln nach Packungsangabe kochen.
Koriander waschen, trocken schütteln und klein hacken.

Die Suppe mit Sojasoße und Limettensaft.

--- 

Quelle: http://web.archive.org/web/20160703181646/https://www.guteguete.at/2015/08/rezept-schnelle-thaisuppe-vegan.html
