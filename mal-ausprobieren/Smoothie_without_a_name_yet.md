# Smoothie without a name... yet

Recommended but not yet mixed

**1 Smoothie**

---

- *2 dl* of plain yoghurt
- *1* banana
- handful of berries (I like frozen strawberries and blueberries)
- half a handful of nuts (soak them overnight so they're nice and soft)
- a bit of kale or spinach

---

blend and enjoy

Source https://tech.lgbt/@tuturto/106804305041917829
