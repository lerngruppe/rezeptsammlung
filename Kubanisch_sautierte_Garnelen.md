# Kubanisch sautierte Garnelen

sautieren (verb) \[zoˈtiːʁən\]: dünn geschnittenes oder zerkleinertes Gargut in wenig Fett in einer Pfanne mit hohem Rand (Sauteuse) oder in einem Wok bei mittlerer bis hoher Hitzeeinwirkung unter ständigem Rühren oder Schwenken kurz angaren oder fertig garen (https://de.wiktionary.org/wiki/sautieren).

Muss definitiv noch ein bisschen angepasst werden.

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *600 g* Garnelen, geschält und entdarmt
- *2* Limetten
- *300 g* Wildreis-Mischung
- *800 ml* Kokosmilch
- *1* Mango
- *1/2* rote Zwiebel
- *1* Chilischote, z.B. Jalapeño, je nach gewünschtem Schärfegrad
- *1* Avocado
- *5* Kochbananen
- *4 EL* Kokosöl
- *6 Zehen* Knoblauch
- *2 TL* Zucker
- *2 TL* Kreuzkümmel
- *2 TL* Paprikapulver
- *15 g* Koriandergrün
- Salz

---

Wenn die Garnelen tiefgekühlt sind über Nacht im Kühlschrank oder vorsichtig im Backofen bei 50°C auftauen lassen.

Die Schale von den Limetten abreiben und den Saft auspressen.  
In einem Wok 2 EL Kokosöl erhitzen.  
Den Reis darin unter ständigem Rühren 2 Minuten sautieren.  
Mit Kokosmilch ablöschen.    
Zucker, Limettenschale und eine Prise Salz einrühren.  
Auf niedriger Hitze langsam kochen.

Knoblauch schälen und fein hacken.  
Die Garnelen mit Kreuzkümmel, Paprikapulver und Knoblauch marinieren.

Für die Mangosalsa die Zwiebel schälen und klein schneiden.  
Die Mango und Chilischote klein schneiden.  
Beides mit der Zwiebel und 4 EL Limettensaft vermengen.

Die Avocado in Stücke schneiden.

Die Garnelen in 1 EL Kokosöl ca. 2 bis 3 Minuten sautieren.  
Dann mit 4 EL Limettensaft ablöschen und weitere 2 bis 3 Minuten braten.  
Dabei aufpassen, dass die Garnelen nicht zu trocken werden.

Die Kochbananen in Scheiben schneiden.  
In 1 EL Kokosöl von beiden Seiten 2 bis 3 Minuten braten.

---

Quelle: https://www.chefkoch.de/rezepte/2894651441793541/Kubanisch-sautierte-Garnelen-mit-Kokosnuss-Reis-Mangosalsa-und-gebratenen-Kochbananen.html
