# Couscous-Halloumi-Salat

30 min

*main_ingredient_grains, main_dish, main_ingredient_salad, arabia_levante, dinner_lunch, diet_vegetarian, budget_above_15_us, budget_under_7_de, budget_under_10_de, to_go, batch_cooking, party_recipes, diet_pescetarian*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *4 TL* Kurkuma
- *6 EL* Olivenöl
- *400 g* Halloumi
- *40 g* Minze, frisch
- *330 g* Couscous
- *2* Limette
- *300 g* Kichererbse, Dose od. Glas
- *500 g* Cherrytomaten
- *2 TL* Kreuzkümmel (Cumin), gemahlen
- Salz
- Pfeffer

---

Limette halbieren und Saft auspressen.

Die Hälfte des Limettensaftes, Olivenöl, Kreuzkümmel, Kurkuma, Salz und Pfeffern verrühren.

Halloumi in Würfel schneiden.

Halloumi mit der Marinade vermengen und marinieren lassen.

Couscous nach Packungsanleitung kochen.

Cherrytomaten waschen und halbieren.

Minze waschen, trocken schütteln und fein hacken.

Eine Pfanne auf mittlerer Stufe heiß werden lassen. Halloumi und etwas von der Marinade hineingeben und ca. 5 min. braten.

Kichererbsen waschen und abtropfen lassen.

 Couscous mit einer Gabel auflockern und mit Olivenöl, Limettensaft und Minze vermengen.

Kichererbsen, Tomaten und Halloumi untermischen. Mit Salz und Pfeffer abschmecken und genießen.

---

Quelle: Jodie (https://blog.kptncook.com/2020/11/05/durfen-wir-vorstellen-unsere-koch-kptns/) via https://sharing.kptncook.com/By9eh0tXVnb
