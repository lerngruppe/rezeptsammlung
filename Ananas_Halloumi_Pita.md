# Ananas-Halloumi-Pita

30 min

*main_ingredient_vegetables, main_dish, italian, dinner_lunch, diet_vegetarian, grilled, spring, summer, calories_under_500, calories_under_600, budget_above_15_us, to_go, diet_pescetarian*

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *1* Ananas
- *500 g* Halloumi
- *200 g* Joghurt, natur
- *1 Zehe* Knoblauch
- *20 g* Minze, frisch
- *1* Limette
- *1* Eisbergsalat
- *500 g* Fladenbrot
- Salz
- Pfeffer
- Zucker

---

Ananas schälen, den holzigen Strunk entfernen und das Fruchtfleisch in Scheiben schneiden.

Halloumi in Scheiben schneiden.

Ananas und Halloumi in einer Pfanne oder auf dem Grill anbraten.

Fladenbrot nach Packungsanweisung aufbacken.

Minze waschen, trocken schütteln und fein hacken.

Knoblauch schälen und klein hacken.

Limette halbieren und Saft auspressen.

Für die Minze-Joghurtsoße Joghurt, Minze, Knoblauch und Limettensaft verrühren. Mit Salz, Pfeffer und einer Prise Zucker abschmecken.

Salat waschen, abtropfen und Blätter abtrennen.

Fladenbrot vierteln, aufschneiden und mit Salat, Ananas und Halloumi füllen. Zum Schluss Minz-Joghurtsoße drüber geben und genießen!

---

Quelle: Marcel (http://vollgut-gutvoll.de/) via https://sharing.kptncook.com/t5JqismXVnb. Auch unter https://vollgut-gutvoll.de/2017/06/02/ananas-thymian-pitas-mit-halloumi/
