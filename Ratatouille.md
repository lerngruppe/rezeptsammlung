# Ratatouille

Südfranzösisches Gericht aus geschmortem Gemüse. Random Gemüse nach Gewürze schmecken lassen.

*french, vegetarian, vegan, halal*

**4 Personen**

---

- *1* große Aubergine
- Salz
- *1* große Gemüsezwiebel
- *5* Zehen Knoblauch
- *2* große Zucchini
- *1 kg* Paprika (2 große rote, 2 große gelbe)
- *1* große Dose geschälte Tomaten
- *2* EL Olivenöl
- *1/2* Tube Tomatenmark
- *3* TL (z.B. Rosmarin, Thymian oder Salbei) Kräuter, z. B. das Pizzagewürz, welches noch nicht in der Repo ist.
- *1* TL Zucker
- Schwarzer Pfeffer

---

### Aubergine vorsalzen

Aubergine waschen, putzen & in mundgerechte Stücke schneiden.  
Auberginenstücke mit Salz würzen  
mindestens 10 Minuten ziehen lassen  
dann gründlich trocken tupfen.

### Gemüse schneiden

Zwiebel schälen in grobe Würfel schneiden.  
Knoblauch schälen und in feine Würfel schneiden.  
Zucchini putzen, waschen und in Würfel schneiden.  
Paprikas entkernen, waschen und in grobe Stücke schneiden.

### Gemüse anbraten & Gewürze hinzugeben

Öl in einem großen Topf erhitzen und Zwiebel, Knoblauch und Zucchini darin anbraten.  
Paprika und Aubergine hinzufügen.  
Alles nochmal etwa 5 Minuten kräftig anbraten.  
Tomatenmark dazugeben und unterrühren,  
mit Salz und Pfeffer würzen.
Geschälte Tomaten mit Kräutern und Zucker hinzufügen.  

### Köcheln

ca. 20 Minuten köcheln lassen,  
bei Bedarf etwas Wasser dazugießen. 

Das Gemüse sollte am Ende der Garzeit noch etwas Biss haben.  
Zum Servieren das Ratatouille nochmals kräftig abschmecken.

**Nährwerte (pro Portion)**:
230 kcal, 9 g Eiweiß, 8 g Fett, 24 g Kohlenhydrate, 9 g Ballaststoffe

**Tipp:**
Dazu passt sehr gut im Ofen gegarter Lachs.

---

Quelle: https://www.ndr.de/ratgeber/kochen/rezepte/rezeptdb6_id-9664_broadcast-1530_station-ndrtv.html
