# Börek-Auflauf

*orientalisch*

**3 [Lerngruppz](README.md#lerngruppz)**

---

- *2* rote Zwiebeln
- *2* Zehen Knoblauch
- *1* Zucchini
- *300 g* Feta
- *500 g* Tiefkühlspinat, aufgetaut
- *2* Eier
- *150 ml* Milch oder vegane Alternative
- *12 bis 14 Blätter* Strudelteig
- *200 g* Kirschtomaten
- *1,5 TL* Schwarzkümmelsamen
- Öl zum Braten
- Salz
- Pfeffer
- Chiliflocken

---

Öl in einer Pfanne erhitzen.  
Zwiebeln und Knoblauch schälen, klein schneiden und anbraten.
Zucchini waschen, klein schneiden und mit den Zwiebeln andünsten.  
Feta klein schneiden.  
Feta und Spinat in die Pfanne geben.  
Mit Salz, Pfeffer und Chiliflocken würzen.

Tomaten halbieren.

Eier und Milch vermengen.  
Mit Salz und Pfeffer würzen.

Eine Auflaufform mit 1 TL Öl einfetten.

Der Auflauf wird folgendermaßen geschichtet:

```
3x Strudelteig
Ei
Tomaten
3x Strudelteig
Ei
Füllung
4x Strudelteig
Ei
Füllung
4x Strudelteig
```

Die Schwarzkümmelsamen darauf streuen und 25 Minuten bei 180°C backen.

---

Quelle: https://eatsmarter.de/rezepte/boerek-lasagne-mit-spinat-und-feta
