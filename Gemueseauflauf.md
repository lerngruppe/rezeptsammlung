# Gemüseauflauf

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *250 g* Reis
- *2* Zwiebeln
- *250 g* Champignons
- *500 ml* Milch
- *3* Paprika
- *1 kleine Dose* Mais
- *200 g* gekochten Schinken
- *2* mittelgroße Fleischtomaten
- *200 g* Raspelkäse
- *1 EL* Kurkuma
- Sonnenblumen- oder Rapsöl
- *2 TL* Paprikapulver
- Muskat
- *4 TL* Gemüsebrühepulver
- *1 EL* Curry
- Petersilie
- Schnittlauch
- Pfeffer
- Salz

---

Reis in Salzwasser kochen.  
Dabei Kurkuma ins Wasser geben.

Zwiebeln würfeln.  
Champignons in Scheiben schneiden.  
Beides in Öl glasig dünsten.  

Paprika klein schneiden.  
Paprika, Paprikapulver, Milch und die Hälfte des Brühepulvers zu den Pilzen geben.  
Mit Muskat abschmecken.  
Ca. 10 Minuten kochen lassen, bis die Paprika bissfest ist.

Schinken klein schneiden.  
Schinken, Mais, den Rest Brühepulver, Curry, Petersilie, Schnittlauch in die Soße geben.

Tomaten klein schneiden.

Den Backofen auf 180° vorheizen (Umluft).

Wenn der Reis und die Paprika gar sind, eine Auflaufform mit Reis, Tomaten und Soße füllen.  
Mit Pfeffer würzen.  
Mit dem Käse bestreuen.

Ca. 20 bis 25 Minuten backen.

---

Quelle: https://www.chefkoch.de/rezepte/626851163008372/Bunter-Gemueseauflauf-mit-Paprika-Champignons-Schinken-und-Reis.html
