# Kabeljau mit Kokoskruste und Erdnusssoße

30 min

*diet_dairy_free, budget_above_15_us, main_dish, budget_above_15_de, baked, main_ingredient_fish, dinner_lunch, diet_glutenfree, calories_under_600, diet_pescetarian*

**2 Portionen**

---

- *0.5 EL* Erdnussbutter
- *0.5 * Ei
- *1.5 EL* Kokosnuss, geraspelt
- *2 * Karotte
- *2 * Frühlingszwiebel
- *0.5 Zehe(n)* Knoblauch
- *5 g* Ingwer
- *0.25 * Chilischote
- *0.5 EL* gelbe Currypaste
- *100 ml* Kokosmilch
- *5 g* Koriander, frisch
- *0.25 EL* Kokosöl
- *0.5 EL* Erdnussöl
- *50 g* Erbse, tiefgefroren
- *110 g* Kabeljaufilet
- Salz
- Pfeffer

---

Fisch auftauen lassen und anschließend mit Küchenpapier trocken tupfen.

Backofen auf 200°C (Ober- und Unterhitze, empfohlen) oder 180°C (Umluft) vorheizen.

Karotten schälen und schräg in dicke Scheiben schneiden.

Frühlingszwiebeln waschen und in ca. 5 cm lange Stücke schneiden.

Erdnussbutter und Ei in eine Schüssel geben und mischen. Mit Salz würzen.

Kokosflocken auf einen Teller geben. Fisch zuerst in Erdnuss-Ei-Mischung dann in Kokosflocken wenden.

Panierten Fisch auf ein mit Backpapier belegtes Backblech legen.

Karotten und Frühlingszwiebeln ebenfalls auf das Backblech geben, mit Erdnussöl beträufeln, mit Salz und Pfeffer würzen, mischen und alles zusammen ca. 15 min. rösten.

Währenddessen Knoblauch schälen und reiben.

Ingwer schälen und reiben.

Chili waschen, entkernen und fein würfeln.

Kokosöl in einem Topf erhitzen und Knoblauch, Ingwer und Chili 1 min. bei mittelhoher Hitze braten.

Currypaste hinzugeben, mischen und weitere 2-3 min. braten.

Kokosmilch, Erbsen und nach belieben Erdnussbutter hinzugeben, mischen und 2-3 min. köcheln lassen.

Währenddessen Koriander waschen, trocken schütteln und grob hacken.

Gerösteten Fisch und Gemüse aus dem Ofen nehmen, mit Sauce auf einen tiefen Teller geben und mit gehacktem Koriander servieren.


---

Quelle: Anna (https://blog.kptncook.com/2020/11/05/durfen-wir-vorstellen-unsere-koch-kptns/) via https://sharing.kptncook.com/X00qAteXVnb
