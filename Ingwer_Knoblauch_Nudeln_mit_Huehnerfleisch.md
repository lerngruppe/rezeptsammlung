# Ingwer-Knoblauch-Nudeln mit Hühnerfleisch

**3 Lerngruppende**

---

- *500 g* Hühnerbrust
- *500 g* Nudeln
- *13* Knoblauchzehen
- *13 cm* Ingwerwurzel, frisch
- *10 Zweige* Thymian
- *0.6 Liter* Hühnerbrühe oder Hühnersuppe
- *30 g* Butter
- *2 EL* Olivenöl
- Salz und Pfeffer

---

Fleisch in mundgerechte Stücke schneiden und in eine Schüssel geben.  
Thymianzweige waschen, trocken schleudern, die Blättchen von den Zweigen zupfen.  
Das Fleisch mit Thymian, Salz, Pfeffer und dem Olivenöl marinieren.   
In einer Pfanne knusprig braten.  
Das Fleisch warm stellen (Backofen 80°). 

Nudeln bissfest kochen. 

Währenddessen Knoblauch und Ingwer schälen und fein hacken.  
In den Bratenrückstand der Pfanne die Butter geben und erhitzen.  
Knoblauch und Ingwer dazugeben, anschwitzen und anschließend mit der Hühnersuppe aufgießen und um ca. 1/3 einkochen lassen (dauert etwa 10-15 min).   
Die Nudeln und das Fleisch beigeben, durchschwenken und noch kurz durchziehen lassen.

---

Mal ausprobieren: Zitronengras und/oder Limettensaft

Quelle: https://www.chefkoch.de/rezepte/1451641250072950/Ingwer-Knoblauch-Nudeln-mit-Huehnerfleisch.html
