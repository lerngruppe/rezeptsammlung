# Hähnchenpfanne

**3 [Lerngruppz](README.md#lerngruppz)**

---

- *300 g* Prinzessbohnen
- *300 g* Brokkoli
- *4* Zwiebeln
- *2* Chilischoten
- *200 g* Kirschtomaten
- *1 Dose* Kidneybohnen
- *1 kg* Hähnchenbrustfilet
- *250 g* Feta oder Hirtenkäse
- Öl zum Braten
- mediterrane Gewürze
- Salz
- Pfeffer

---

Wenn die Prinzessbohnen und der Brokkoli gefroren sind, in einer Pfanne auftauen.
Chilischoten klein hacken und in die Pfanne geben.  
Zwiebeln und Knoblauch schälen, klein schneiden und in die Pfanne geben.  
Tomaten vierteln und in die Pfanne geben.  
Kidneybohnen abgießen und in die Pfanne geben.  
Hähnchen klein schneiden und in die Pfanne geben.  
Braten, bis das Hähnchen durch ist.
Mit Salz, Pfeffer und anderen Gewürzen abschmecken

Zum Servieren mit Käse bestreuen.

---

Quelle: https://www.chefkoch.de/rezepte/4028541619965113/Wuerzige-Haehnchen-Proteinpowerpfanne-mit-Hirtenkaese.html
