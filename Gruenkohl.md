# Grünkohl

Klassisch Norddeutsch.

*savoury, german, nonhalal*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1,5 kg* Grünkohl, frisch oder tiefgefroren
- *800 g* Kartoffeln
- Haferflocken
- Senf
- Salz
- Pfeffer

## Fleisch (2 bis 3 der folgenden)
- *800 g* Kasseler
- *4* Kohlwürste oder geräucherte Mettenden
- *4* Bregenwürste
- *4* Pinkelwürste

---

Grünkohl in einen großen Topf geben und mit Salz und Pfeffer würzen.  
Einmal aufkochen.  
1,5 Stunden mit Deckel bei geringer Hitze kochen, dabei zwischendurch aufpassen, dass es nicht anbrennt und evtl. Wasser dazugeben.

Das Stück Kasseler dazugeben.  
Nochmal 20 Minuten kochen.

Die Kartoffeln schälen.

Die Kartoffeln in einem anderen Topf kochen.  
Die Würste in den Grünkohl geben.  
Den Grünkohl mit Senf würzen.  
Mit etwas Haferflocken kann man den Grünkohl andicken.  
Alles nochmal 40 Minuten kochen.

Dazu passt norddeutsches Bier.
