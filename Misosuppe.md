# Misosuppe

*japanisch*

**5 [Lerngruppz](README.md#lerngruppz)**

---

- *1 L* Rinderbrühe
- *2* Möhren
- *4 EL* Misopaste
- *1 Blatt* Nori (getrocknete Algen)
- *50 g* Ingwer
- *1* Porree
- *150 g* Champignons
- *1 Bund* Radieschen
- *1* Zwiebel
- *1 Bund* Frühlingszwiebeln
- *5* Eier
- *500 g* Fleisch (Rind, Geflügel, Ente, ...)
- *500 g* asiatische Nudeln, z.B. Soba, Udon, Mie

---

Rinderbrühe kochen.  
Möhren in dünne Scheiben schneiden und in der Brühe kochen.  
Misopaste und Nori dazugeben.

Porree, Champignons, Radieschen und Frühlingszwiebeln waschen.  
Ingwer schälen.  
Porree, Champignons, Radieschen, Zwiebel und Ingwer klein schneiden und in die Brühe geben.  
Frühlingszwiebeln klein schneiden. Einen Teil des Grünen beiseitelegen, den Rest in die Brühe geben.  
Das Fleisch in kleine Stücke schneiden und in die Brühe geben.  
Die Suppe kochen lassen, bis das Fleisch gar ist.

Die Nudeln nach Packungsanweisung kochen.

Die Eier wachsweich kochen (6:45 Minuten).  
Eier halbieren.

---

Zum Servieren Nudeln in eine Suppenschüssel legen und Suppe darüber tun.  
Ein halbes Ei oben drauf legen und mit Frühlingszwiebel garnieren.
