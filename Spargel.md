# Spargel

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1,4 kg* geschälten Spargel
- *1,5 kg* Frühkartoffeln
- *600 ml* Knorr Sauce Hollandaise aus dem Tetrapak
- *1 kg* Chicken Nuggets oder Hähnchenschnitzel, paniert, tiefgekühlt
- Salz

---

Kartoffeln in Salzwasser kochen.
Spargel in Salzwasser kochen.

Ca. 20 Minuten warten

Chicken Nuggets nach Packungsanweisung im Backofen backen.

Sauce in der Mikrowelle erwärmen.