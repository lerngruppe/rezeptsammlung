# Grünkohl mit Erdnussbutter

30 min

*main_ingredient_vegetables, main_ingredient_rice, main_dish, dinner_lunch, diet_vegan, diet_vegetarian, diet_dairy_free, winter, fall, budget_under_7_de, budget_under_10_de, comfort_foot, party_recipes, diet_pescetarian*

**2 Portionen**

---

- *10 EL* Pflanzenöl
- *1* Chilischote
- *60 g* Erdnussbutter
- *250 g* Basmati-Reis
- Salz
- Pfeffer
- *3 Zehen* Knoblauch
- *2* Speisezwiebel
- *500 g* Grünkohl
- *0.5 l* Gemüsebrühe
- *0.5 TL* Kreuzkümmelsamen

---

Reis nach Packungsanleitung kochen.

Grünkohl waschen, trocken schütteln, Strunk entfernen und fein hacken.

Zwiebel schälen und fein würfeln.

Knoblauch schälen und fein würfeln.

Chili waschen, entkernen und fein würfeln.

Öl in einer großen Pfanne erhitzen, Zwiebel und Knoblauch darin ca. 3 min. anschwitzen.

Grünkohl in die Pfanne geben und ca. 2 min. anschwitzen, bis der Kohl etwas zusammenfällt.

Brühe dazugeben, mit Salz und Pfeffer würzen und ca. 5 min. köcheln lassen.

Erdnussbutter dazugeben und gut verrühren.

Erdnuss-Grünkohl mit Reis genießen.

---

Quelle: Maria &amp; Perry (https://www.resipis.de/) via https://sharing.kptncook.com/zGDHzgaXVnb
