# Lasagne Bolognese

*Hauptspeise*

**4 Portionen**

---

- Lasagneplatten für 3 Schichten, gegart
- *500 g* Hackfleisch
- *3 EL* Olivenöl
- *1 große* Möhre
- *1 Stück* Sellerie oder Staudensellerie
- *2 große* Zwiebeln
- *2 Zehen* Knoblauch
- *300 ml* Weißwein
- *400 ml* Gemüsebrühe
- *3 EL* Tomatenmark
- *3 EL* Butter
- *4,5 EL* Mehl
- *750 ml* Milch
- *100 g* Parmesan, frisch gerieben
- *1* Packung Raspelkäse
- Zucker
- Salz und Pfeffer
- Muskat


---

Möhre, Sellerie, Zwiebel und Knoblauch putzen und in kleine Würfel schneiden.  
Das Gemüse in Öl anbraten und wieder aus der Pfanne nehmen.  

Das Hackfleisch in der gleichen Pfanne anbraten.  
Das Gemüse hinzugeben und mit dem Wein ablöschen.  
Den Wein fast verkochen lassen und fast verkochen lassen.  
Tomatenmark dazugeben und mit Salz, Pfeffer und einer Prise Zucker würzen.  
Mit der Gemüsebrühe auffüllen.  
Mit Deckel sämig einkochen lassen.

Für die Béchamelsoße die Butter in einem Topf schmelzen.  
Das Mehl einrühren und kurz anschwitzen.  
Unter Rühren nach und nach die Milch zugießen.  
Mit Muskat und Salz abschmecken und einmal aufkochen lassen.

Den Backofen vorheizen: 180 °C Ober-/Unterhitze.

Eine Auflaufform fetten und nach folgenden Schema füllen:

```
   Raspelkäse
   Lasagne
   Bolognese
   Parmesan
   Bechamel
   Lasagne
   Bolognese
   Parmesan
\  Bechamel  /
 \ Lasagne  /
  ----------
```

Ca. 20 Minuten backen.

---

Quelle: https://www.chefkoch.de/rezepte/1112181217260303/Lasagne-Bolognese.html
