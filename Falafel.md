# Falafel

**6 [Lerngruppz](README.md#lerngruppz) wenn man Beilagen dazu hat**

---

- *2* Zwiebeln
- *3 Zehen* Knoblauch
- *2 Scheiben* Brot
- *500 g* Kichererbsen, über Nacht in Wasser eingeweicht
- *1 Bund* Petersilie
- *4 EL* Mehl
- *2 TL* Backpulver
- *4 TL* Korianderpulver
- *4 TL* Kreuzkümmel
- Salz
- Pfeffer
- *1 L* Öl zum Frittieren, circa

---

Zwiebeln und Knoblauch schälen und grob hacken.  
Das Brot zerkrümeln.  
Petersilie klein hacken.  

Die Kichererbsen, Zwiebeln, Knoblauch, Brot, Petersilie, Mehl und Backpulver pürieren.  
Mit Koriander, Kreuzkümmel, Salz und Pfeffer abschmecken.

Das Öl in einer Fritteuse oder in einem kleinen Topf erhitzen.  
Aus dem Püree etwa Walnussgroße Bällchen formen.  
Diese 4 bis 5 Minuten frittieren, bis sie von außen dunkelbraun sind.

Die frittierten Falafel auf Küchenpapier abtropfen lassen.

---

Quelle: https://www.chefkoch.de/rezepte/779611181045749/Falafel.html
