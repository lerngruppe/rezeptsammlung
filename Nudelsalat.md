# Nudelsalat

schnell und einfach

*zum Grillen*

**6 [Lerngruppz](README.md#lerngruppz)**

---

- *300 g* Nudeln (roh)
- *300 g* Kirschtomaten
- *1* Gurke
- *300 g* gekochten Schinken oder Hühnerwurst
- *250 g* Mayo
- *200 g* Saure Sahne
- *1 EL* Balsamico
- frisches Basilikum
- Salz
- Pfeffer

---

Nudeln nach Packungsanweisung kochen und abgießen.

Die Tomaten halbieren.  
Die Gurke und die Wurst in Stücke schneiden.

Mayo und saure Sahne mit Balsamico verrühren.  
Mit Basilikum, Salz und Pfeffer würzen.

Alles gut verrühren.
