# Hackfleischauflauf

Nichts Schnelles für die Zeit nach Feierabend, aber lecker - die Mühe und Zeit lohnt sich wirklich.

*auflauf*

**4 [Lerngruppz](README.md#lerngruppz)**

--- 

- *10* große Kartoffel(n)
- *6* Fleischtomate(n) oder große Tomaten
- *2* Zwiebel(n)
- *5* mittelgroße Zucchini
- *800 g* Hackfleisch, gemischtes
- *3* kl. Dose/n Tomatenmark
- *250 ml* süße Sahne
- *2* Eigelb
- *150 g* Käse (Emmentaler), geriebener
- Öl (Sonnenblumenöl)
- Salz und Pfeffer
- Paprikapulver, edelsüß
- Basilikum, getrocknetes
- n. B. Wasser 

---

Die rohen Kartoffeln schälen, in kleine Würfelchen schneiden und in einer Pfanne bzw. zwei Pfannen in reichlich Sonnenblumenöl so lange braten, bis sie goldbraun und knusprig sind. Mit einer Schaumkelle aus der Pfanne nehmen. In die Auflaufform ausbreiten und salzen.

Die Tomaten in kochendes Wasser geben, abschrecken und häuten. Danach in Scheiben schneiden. Dann die Zwiebeln schälen und kleinhacken. Die Zucchini waschen und in Scheiben schneiden.

In einem großem Topf Zwiebeln in heißem Öl anbraten. Hackfleisch zugeben und mit Salz und Pfeffer würzen. Das Hackfleisch so lange braten, bis es durch ist, dabei zerkleinern. Das Tomatenmark zugeben und unterrühren. Nochmals salzen, pfeffern und Paprikapulver zugeben, dabei 5 Min. ständig rühren (bis Säure aus dem Tomatenmark entfernt ist). Jetzt die Zucchini zugeben und unterrühren. Mit soviel Wasser ablöschen, dass die Flüssigkeit sichtbar ist (wenn zuviel - einfach länger kochen lassen). Das Basilikum zugeben und solange kochen lassen, bis die Flüssigkeit eingedickt ist und die Zucchini weich sind.

Jetzt in der Auflaufform auf die Hackfleisch-Zucchinimischung auf den Kartoffeln verteilen. Die Tomatenscheiben darauf schichten. Mit Salz, Pfeffer und sichtbar viel Basilikum würzen.

Die Sahne mit Eigelb verquirlen. Salz, Pfeffer und Muskatnuss zugeben und auf den Auflauf geben. Den Emmentaler nach Bedarf darauf verteilen.

Im vorgeheizten Backofen bei 180°C ca. 30 Minuten backen, bis der Käse goldbraun ist

---

inspiriert von: https://www.chefkoch.de/rezepte/955371201105352/Hackfleisch-Zucchini-Kartoffel-Tomatenauflauf.html
