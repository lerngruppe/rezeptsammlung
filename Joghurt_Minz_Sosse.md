# Joghurt-Minz-Soße

**500 Gramm, 6 [Lerngruppz](README.md#lerngruppz)**

---

- *500 g* Sahnejoghurt oder griechischer Joghurt
- *3 EL* Zitronensaft
- *3 EL* Petersilie
- *3 EL* Minze gehackt
- Salz
- Pfeffer

--- 

Den Joghurt mit Salz, Pfeffer, Zitronensaft, Minze und Petersilie verrühren.

---

Quelle: https://www.chefkoch.de/rezepte/1178021223968697/Tuerkische-Hackbaellchen-mit-Joghurtsauce.html
