# Deftige Laugen-Petersilien-Knödel
### aus dem Ofen, dazu cremiger Porree-Rahm (Ca. 40 Min)

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *8* Laugenstangen
- *12-16* Porree Stangen
- *100 g* geraspelter Käse (Je nach Wunsch der Würzung: Gouda oder Bergkäse)
- *600-800 g* Sahne
- *1 Bund* Petersilie (Krause)
- *500 g* Milch
- *50 g* Butter
- *3 TL* Gemüsebrühe
- *1 EL* Butter zum Braten
- Salz
- Pfeffer

---

1. Wasche Gemüse und Kräuter ab. Heize den Backofen auf 220°C Ober-/Unterhitze (200 °C Umluft) vor. Zum Kochen benötigst Du außerdem 1 große Pfanne, 1 kleinen Topf, eine Auflaufform und 1 große Schüssel. 
Milch + Gemüsebrühe + Butter in einem kleinen Topf erwärmen. Währenddessen die Laugenstangen in 1 cm große Würfel schneiden. Petersilie fein hacken.  

2. Laugenstangenwürfel, Gewürzmischung und gehackte Petersilie in eine große Schüssel geben, mit warmer Milch übergießen, verrühren, mit Salz und Pfeffer würzen und abgedeckt etwas abkühlen lassen. In der Zwischenzeit mit dem Rahmgemüse beginnen.  

3. Wurzel vom Porree abschneiden und Porreestange in feine Ringe schneiden.  

4. Mit den Händen geraspelten Bergkäse unter die Laugenmasse mischen. Mit feuchten Händen 12 Knödel formen, auf Backpapier auf ein Backblech oder in eine Auflaufform geben und ca. 15 Min. im Backofen backen.  

5. In einer großen Pfanne 1 EL Butter bei mittlerer Hitze erwärmen und Porreeringe darin 4 – 5 Min. braten. Mit Sahne ablöschen. Rahmgemüse mit Salz und Pfeffer abschmecken.  

6. Semmelknödel aus dem Backofen nehmen und kurz abkühlen lassen. Rahm-gemüse auf Teller verteilen, je 3 Knödel darauf anrichten und genießen.  

---

Nährwertangaben:  
- Energie (kJ): 2778 kJ  
- Energie (kcal): 664 kcal  
- Fett: 31.0 g  
- davon gesättigte Fettsäuren: 18.0 g  
- Kohlenhydrate: 71 g  
- davon Zucker: 12.0 g  
- Eiweiß: 16 g  
- Salz: 1.0 g  
