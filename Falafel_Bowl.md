# Falafel-Bowl

**6 [Lerngruppz](README.md#lerngruppz)**

---

- *6 Portionen* [Falafel](Falafel.md)
- *400 g* Rotkohl
- *1* Romanasalat
- *3* Tomaten
- *1* Gurke
- *500 g* [Joghurt-Minz-Soße](Joghurt-Minz-Sosse.md)
- *15 g* Koriander
- Salz
- *2 EL* Balsamico
- *2 EL* Olivenöl

---

Rotkohl waschen und klein schneiden.  
Mit 1 TL Salz, Balsamico und Olivenöl vermengen.

Romanasalat waschen und klein schneiden.

Koriander hacken.  
Tomaten und Gurke waschen, klein schneiden und mit der Hälfte des Korianders vermengen.

In die Joghurtsoße die andere Hälfte des Korianders einrühren.

---

Quelle: https://www.chefkoch.de/rezepte/3383671503423417/Orientalische-Bowl-mit-Falafel-und-Joghurtsauce.html
