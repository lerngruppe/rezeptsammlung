
# Hackbraten mit Käsefüllung

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *1* Brötchen (alternativ: Semmelbrösel)
- *1* Ei
- *500 g* Hackfleisch
- *1 kg* Kartoffeln
- *200 g* Raspelkäse
- *200 ml* Sahne
- Petersilie
- Salz
- Pfeffer
- Muskat

---

Brötchen zerkleinern.  
Mit dem Hack, Ei, Petersilie, Salz, Pfeffer und Muskat vermengen und zu einem großen Leib formen.  
In die Mitte einer großen Auflaufform geben.

150 g vom Käse in das Fleisch drücken und oben wieder verschließen.

Die Kartoffeln schälen und dünn hobeln.

Den Backofen auf 180° Umluft vorheizen.

Die Sahne mit Salz, Pfeffer und Muskat würzen und mit den Kartoffeln vermengen.  
Um das Hack verteilen.  
Mit dem Rest Käse bestreuen.

60 - 90 Minuten backen.
