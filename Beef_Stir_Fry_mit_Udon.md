# Beef Stir Fry mit Udon

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *600 g* Rinderfilet
- *4* rote Zwiebeln
- *1 kg* Brokkoli
- *6* Möhren
- *200 g* Zuckerschoten
- *400 g* Udon-Nudeln
- Olivenöl
- *200 ml* Teriyaki-Soße
- etwas Röstzwiebeln

---

Rinderfilet in Streifen schneiden und in Olivenöl scharf anbraten. Es darf innen noch rosa sein.
Aus der Pfanne nehmen und beiseite stellen.

Zwiebeln schälen, klein scheiden und anbraten.
Brokkoli waschen und in Röschen schneiden.
Möhren schälen und in dünne scheiben schneiden.
Zuckerschoten waschen.
Das Gemüse in der Pfanne etwa 10 bis 15 Minuten garen, dabei eventuell Öl nachgeben.

Die Nudeln nach Packungsanleitung kochen.

Das FLeisch zurück in die Pfanne geben.
Die Teriyaki-Soße dazugeben und gut verteilen.

Mit Röstzwiebeln servieren.

---

Quelle: https://www.chefkoch.de/rezepte/3294631489149958/Beef-Stir-Fry-mit-Udon-Nudeln.html
