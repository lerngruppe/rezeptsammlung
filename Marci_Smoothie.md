# Marci Smoothie

Smoothie, von Marci empfohlen.

![](img/marci-smoothie.jpg)

*protein, quick, halal*

**1 servings**

---

- *2* Bananen
- *1 Tasse* Haferflocken
- Paar Sonnenblumenkerne
- Paar Leinsamen (die muss man aber schroten oder geschroten kaufen, sonst werden die nicht verdaut)
- ein Löffel Erdnussbutter 
- optional: im Sommer Erdbeeren und/oder Heidelbeeren
- Sojamilch

---

Kann dir auch den Smoothie empfehlen, den ich mir regelmäßig zum Frühstück mache:
2 Bananen
1 Tasse Haferflocken
Paar sonnenblumenkerne
Paar leinsamen (die muss man aber schroten oder geschroten kaufen, sonst werden die nicht verdaut)
Eventuell ein Löffel erdnussbutter und im Sommer Erdbeeren und/oder Heidelbeeren
Mit Sojamilch dann mixxen
Haferflocken haben relativ viel Eiweiß und Ballaststoffe und finde die lecker. Sojamilch kann man natürlich auch mit Mandel oder Hafermilch ersetzen je nachdem welche pflanzliche Milch einem am besten schmeckt ;)

Quelle: https://broomis.slack.com/archives/C02F5CDNR2Q/p1632324101005900 (non-public)
