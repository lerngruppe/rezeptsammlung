# Erbsen-Pancakes

Man benötigt einen guten Mixer.

Dauert ca. 1 Stunde.

*hipster*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1* unbehandelte Zitrone
- *200 g* griechischer Joghurt
- *4 EL* Olivenöl
- *4 TL* Honig
- *400 g* Feta
- *500 g* tiefgefrorene Erbsen
- *8* Eier
- *250 g* Mehl
- Salz
- Pfeffer
- Sonnenblumen oder Rapsöl

---

Erbsen aus dem Gefrierschrank nehmen.

Die Schale der Zitrone abreiben und den Saft auspressen.  
Für die Soße die Zitronenschale, Zitronensaft, Joghurt, Olivenöl und Honig mischen.  
Mit Salz und Pfeffer abschmecken.

Feta klein schneiden.  
Für den Teig den Feta mit den angetauten Erbsen und Eier pürieren.  
Danach das Mehl dazumixen.  
2 EL von der Soße dazugeben.  
Mit Salz und Pfeffer abschmecken.

Mit einer Suppenkelle Teigkleckse in einer Pfanne in Öl portionieren.  
Von beiden Seiten braten.

Die Pancakes können im Backofen bei 50°C warm gehalten werden.

---

Quelle: via KptnCook, Rezept nicht mehr online
