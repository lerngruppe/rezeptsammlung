
# Spaghetti aglio e olio

IPA: \[spaˈgetti aʎʎo e ɔːljo\]

*mediterran, scharf*

**3 [Lerngruppz](README.md#lerngruppz)**

---

- *500 g* Spaghetti
- *250 g* Cherrytomaten
- *2* Chilischoten
- *4 Zehen* Knoblauch
- *125 g* Rucola
- Olivenöl
- Parmesan

---

Spaghetti nach Packungsanleitung kochen.

Olivenöl in einer Pfanne vorsichtig erhitzen.  
Chilischoten sehr fein hacken und anbraten.  
Den Knoblauch schälen und mit in der Pfanne anbraten.  
Tomaten vierteln und in die Pfanne geben.  

Rucola waschen und grob hacken.

Mit Parmesan servieren.

---

Quelle: https://www.chefkoch.de/rezepte/1020941207311518/Pasta-aglio-olio-mit-Rucola-und-Tomaten.html
