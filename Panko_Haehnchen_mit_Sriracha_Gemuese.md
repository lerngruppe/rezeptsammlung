# Panko-Hähnchen mit Sriracha-Gemüse

*asian*, *helal*

**4 Portionen**

---

- *2 Stück* Hähnchenbrust
- *5 EL* Mehl
- *2* Eier
- *8 EL* Panko-Paniermehl
- *400 g* Reis
- *1 kleiner Kopf* Brokkoli
- *1* Aubergine
- *100 g* Sojasprossen
- *2 EL* Kokosöl
- *100 ml* Teriyaki-Soße
- *50 ml* Sriracha-Soße
- Salz
- Pfeffer

---

Hähnchenbrust in Streifen schneiden.  
Mit Salz und Pfeffer würzen.  
In Mehl, Ei und Pankomehl panieren.

Öl in einer Fritteuse oder einem kleinen Topf auf 170°C erhitzen.  
Die Hähnchenstücke frittieren.

Reis nach Packungsanweisung kochen.

Währenddessen Brokkoli, Aubergine und Champignons klein schneiden.  
Das Gemüse in einem Topf in Kokosöl anbraten.  
Die Sojasprossen hinzugeben.  
Teryaki-Soße und Sriracha-Soße dazugeben und das Gemüse darin schwenken.

---

Quelle: https://www.rewe.de/rezepte/pankohaehnchen-sriracha-gemuese/