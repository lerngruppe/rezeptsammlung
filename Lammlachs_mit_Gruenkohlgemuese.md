# Lammlachs mit Grünkohlgemüse

**2 Portionen**

---

- *300 g* Lammlachs
- *1* Zwiebel
- *2* Knoblauchzehen
- *500 g* Grünkohl (alternativ die halbe Menge tiefkegühlten)
- *250 g* Weiße Bohnen
- *1 Zweig* Rosmarin
- *2 EL* Kokosfett (z.B. aus einer nicht geschüttelten Dose Kokosmilch)
- *250 ml* Gemüsebrühe
- Schale von einer Zitrone
- Sonnenblumen- oder Rapsöl
- Salz
- Pfeffer

---

Den Lammlachs mit Salz und Pfeffer würzen.

Zwiebel und Knoblauch klein schneiden.  

Den Grünkohl waschen.  
Die harten Strünke herauschneiden.  
Den Grünkohl klein schneiden.  

Die Hälfte vom restlichen Knoblauch mit der Zwiebel in einem Topf in Öl anbraten.  
Grünkohl, Bohnen und 125 ml der Brühe in den Topf geben und 10 Minuten dünsten.

Das Kokosfett in einer Pfanne erhitzen.  
Den Lammlachs darin mit dem Rosmarin und dem Rest Knoblauch auf beiden Seiten 5 Minuten braten.  
Mit dem Rest Gemüsebrühe ablöschen.  
Zugedeckt weitere 5 Minuten garen.  

Den Bratensud als Soße anrichten.

---

Quelle:

```bibtex
@book{Riedl2018,
    title = {Die Ernährungs-Docs},
    subtitle = {Wie Sie mit der richtigen Ernährung Krankheiten vorbeugen und heilen},
    author = {Matthias Riedl and Anne Fleck and Jörn Klasen and Britta Probol and Annette Willenbücher},
    year = {2018},
    pagecount = {248},
    isbn = {9783898838610},
    publisher = {ZS Verlag},
    edition = 5
}
```
&nbsp;