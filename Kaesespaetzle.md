# Käsespätzle

*schwäbisch*

**3 Personen**

---

- *500 g* Spätzle
- *2* Zwiebeln
- *200 g* Schmelzkäse Gouda
- *200 g* Sahne
- *125 g* Raspelkäse
- Gewürze nach Belieben, z.B. Paprika & Chili oder Kräuter der Provence

---

Wenn die Spätzle noch gekoche werden müssen, in einem Topf mit Salzwasser kochen.

Zwiebeln würfeln und in Butter anbraten.  
Spätzle dazugeben.

Schmelzkäse und Sahne einrühren.

Mit Gewürzen abschmecken.

Den Raspelkäse auf den Spätzle verteilen und schmelzen lassen.
