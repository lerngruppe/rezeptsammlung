# Enchiladas

Enchilada mit Spinat-Bohnen-Füllung. Inspiriert von [AberDerBart](https://github.com/AberDerBart/recipes/blob/master/enchiladas.md).

![](img/enchiladas.jpg)

*vegetarisch, mexikanisch, halāl*

**3 Personen**

---


- *3 EL* Olivenöl
- *6 TL* Kreuzkümmel
- *4 EL* Mehl
- *6 EL* Tomatenmark
- *400 ml* Gemüsebrühe
- Salz
- Pfeffer
- *300 g* geriebener Käse
- *400 g* schwarze Bohnen (Dose)
- *300 g* Blattspinat
- *330 g* Mais (Dose)
- *6* Frühlingszwiebeln
- *8* Mais-Tortillas

---

Den Ofen auf 200°C vorheizen.

## Sauce

Öl erhitzen, *4 TL* Kreuzkümmel, Mehl und Tomatenmark hinzufügen und unter Rühren anbraten.
Gemüsebrühe einrühreren und zum Kochen bringen.
Hitze reduzieren und einkochen bis die Sauce leicht dick wird.
Mit Salz und Pfeffer würzen.

## Füllung

![](img/enchiladas-fuellung.jpg)

Die Frühlingszwiebeln in Scheiben schneiden, grünen Teil zur Seite stellen.
In einer Schüssel 2/3 des Käses, Bohnen, Spinat Mais und weißen Teil der Frühlingszwiebeln mit *2 TL* Kreuzkümmel, Salz und Pfeffer würzen und vermischen.

## Tetris in Auflaufform

Die Füllung auf die Tortillas verteilen, Tortillas eng aufrollen und dicht an dicht in eine Auflaufform legen.
Den verbleibenen Käse auf den Tortillas verteilen und Sauce darüber gießen.
Bei 200°C 15-20 Minuten backen, bis es heiß ist und Blasen wirft.
5 Minuten abkühlen lassen und mit grünem Teil der Frühlingszwiebeln garniert servieren.

