# Spargelcremesuppe

**1 [Lerngruppz](README.md#lerngruppz)**

---

- *1 kg* Spargel, die Schale davon
- *1 L* Wasser, in dem Spargel gekocht wurde, alternativ neues Wasser
- *500 g* Butter
- *2 EL* Mehl
- *1* Eigelb
- *100 ml* Sahne
- *1 EL* Hühnerbrühepulver
- Salz
- Pfeffer
- Muskat
- Zitronensaft
- Petersilie

---

Die Spargelschalen im Wasser ca. 20 Minuten kochen.

In einem anderen Topf Butter schmelzen.  
Das Mehl mit einem Schneebesen einrühren.  
Nach und nach den Spargelsud dazugeben.  
10 Minuten köcheln lassen.  
Mit Hühnerbrühe, Salz, Pfeffer, Muskat und Zitronensaft abschmecken.

Das Eigelb mit der Sahne verrühren und in die Suppe geben.

Mit Petersilie servieren.

---

Quelle: https://www.chefkoch.de/rezepte/3129641466249237/Spargelcremesuppe-aus-Resten.html
