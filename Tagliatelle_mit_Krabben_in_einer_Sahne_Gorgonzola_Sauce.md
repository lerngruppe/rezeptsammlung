# Tagliatelle mit Krabben in einer Sahne-Gorgonzola-Sauce

**4 Portionen**

---

- *500 g* Bandnudeln (grüne Tagliatelle)
- *150 g* Krabben
- *200 g* Gorgonzola
- *200 ml* Sahne
- *200 g* Käse (geriebener Emmentaler)
- *2* Knoblauchzehen
- etwas Milch
- Salz
- Pfeffer

---

Die grünen Tagliatelle wie gewohnt in Salzwasser kochen. Wenn die Nudeln fertig sind, abkühlen lassen und später die Krabben unterrühren.

Denn Gorgonzola Käse in kleine Stücke schneiden, Butter in einem Topf erhitzen und den Käse darin schmelzen lassen. Dann die Sahne und etwas Milch dazugeben, Knoblauch hineinpressen, mit Salz und Pfeffer abschmecken und die Sauce etwas köcheln lassen.

Die Nudeln mit den Krabben in eine Auflaufform geben, die Gorgonzola Sauce darübergießen und zum Schluss den gerieben Käse darüber streuen.

Im vorgeheizten Ofen bei 200° 20 Minuten backen.

---

Quelle: https://www.chefkoch.de/rezepte/691021171621598/Tagliatelle-mit-Krabben-in-einer-Sahne-Gorgonzola-Sauce.html