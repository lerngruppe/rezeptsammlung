# Spanische Tortilla

Im Prinzip Rührei mit viel Kartoffeln.  

*spanisch*

**3 Portionen**

--- 

- *1 kg* Kartoffeln, festkochend (die Menge bezieht sich auf geschälte Kartoffeln)
- *4* Zwiebeln
- *10* Eier (Größe egal)
- Sonnenblumen- oder Rapsöl
- *2 EL* (circa) Gewürze, z.B. Petersilie, Muskat, Thymian
- Salz
- Pfeffer

---

### Gemüse kochen

Kartoffeln in kleine Würfel schneiden (keine Seite länger als 5mm).  
Zwiebeln in Ringe schneiden.  
Beides in viel Öl anbraten.
Ca. 25 Minuten bei geringer Hitze kochen (nicht braten, die Kartoffeln sollen nicht braun werden!). Eventuell Öl nachgeben.

### Eier

Eier mit Salz, Pfeffer und Gewürzen verrühren.  
Zum Gemüse geben.

### Verfestigen

Bei wenig Hitze das Ei stocken lassen, dabei nicht umrühren!
Nach etwa 8 Minuten sollte der Rand sichtbar fest werden.  
Dann die Tortilla wenden, indem man einen großen Teller, Schneidebrett oder Backblech auf die Pfanne zu legen und beides schnell umzudrehen. Danach die Tortilla wieder in die Pfanne gleiten lassen.
Nochmal etwa 5 Minuten garen.

---

Quelle: *Mediterran*, Parragon Books Ltd, ISBN 978-1-4075-7519-3, S. 160
