#!/usr/bin/env python3
from recipemd.data import RecipeParser, RecipeSerializer, multiply_recipe
from pathlib import Path
import dataclasses
from decimal import Decimal
from tempfile import mkdtemp
import subprocess
from datetime import date


def get_markdown_for_recipe_file(file):
    rp = RecipeParser()
    result = ""
    with file.open() as f:
        print(f"parsing {f.name}")
        recipe = rp.parse(f.read())
    # intro
    intro_recipe = dataclasses.replace(
        recipe, yields=[], ingredients=[], ingredient_groups=[], instructions=None
    )
    result += RecipeSerializer().serialize(intro_recipe)
    # ingredients
    fake_title = "REMOVE_THIS"
    ingredients_recipe = dataclasses.replace(
        recipe, title=fake_title, description=None, tags=[], instructions=None
    )
    recipe_half = multiply_recipe(ingredients_recipe, Decimal('0.5'))
    recipe_double = multiply_recipe(ingredients_recipe, 2)
    column_start = r'`\Begin{minipage}[t]{0.3\textwidth}`{=latex}'
    column_between = r'`\hfill`{=latex}'
    column_end = '\n\n' + r'`\End{minipage}`{=latex}'
    result += column_start
    result += RecipeSerializer().serialize(recipe_half).replace("# " + fake_title, "")
    result += column_end
    result += column_between
    result += column_start
    result += RecipeSerializer().serialize(ingredients_recipe).replace("# " + fake_title, "")
    result += column_end
    result += column_between
    result += column_start
    result += RecipeSerializer().serialize(recipe_double).replace("# " + fake_title, "")
    result += column_end
    # description
    desc_recipe = dataclasses.replace(
        recipe, title=fake_title, description=None, tags=[], yields=[], ingredients=[], ingredient_groups=[]
    )
    desc_string = RecipeSerializer().serialize(desc_recipe)
    desc_string = desc_string.replace("# " + fake_title, "")
    desc_string = desc_string.lstrip()
    if desc_string.startswith("---"):
        desc_string = desc_string.replace("---", "", 1)
    desc_string = desc_string.lstrip()
    result += "\n\n" + desc_string
    return result


today_iso = date.today().isoformat()
md_input = (
    r"---" '\n'
    r"title: Rezeptsammlung" '\n'
    r"author: Die Lerngruppe™" '\n'
    f"date: {today_iso}" '\n'
    r"toc: true" '\n'
    r"toc-depth: 0" '\n'
    r"documentclass: book" '\n'
    r"classoption: ['openany', '10pt']" '\n'
    r"geometry: ['margin=2cm']" '\n'
    r"papersize: a4" '\n'
    r"colorlinks: true" '\n'
    r"mainfont: CMU Serif" '\n'
    r"monofont: FreeMono" '\n'  # for box-drawing characters
    r"header-includes: " '|\n'
    r"    `\usepackage{fvextra}\fvset{breaklines=true,fontsize=\small}`{=latex}" '\n'
    # ctex for chinese support
    r"    `\usepackage[UTF8,scheme=plain]{ctex}`{=latex}" '\n'
    r"    `\usepackage{enumitem}`{=latex}" '\n'
    r"    `\setlist[itemize]{leftmargin=*}`{=latex}" '\n'
    # https://github.com/jgm/pandoc/issues/2453s
    r"    `\let\Begin\begin\let\End\end`{=latex}" '\n'
    r"..." '\n'
)

for file in sorted(Path(".").glob("*.md")):
    # skip README.md and CONTRIBUTING.md
    if file.stem.isupper() or file.name.startswith("_"):
        continue
    md_input += get_markdown_for_recipe_file(file)
    md_input += '\n\n'

repo_root_path = Path(".").resolve()
tmpdir = Path(mkdtemp())
md_path = tmpdir.joinpath('Rezeptsammlung.md')
tex_path = tmpdir.joinpath('Rezeptsammlung.tex')
pdf_path = repo_root_path.joinpath('Rezeptsammlung.pdf')
with md_path.open('w') as f:
    f.write(md_input)
subprocess.run(
    [
        "pandoc",
        md_path,
        "--standalone",
        "--from", "markdown+autolink_bare_uris",
        "--resource-path", f".:{repo_root_path}",
        "-o", tex_path
    ],
    check=True
)
latex_process = subprocess.run(
    ["latexmk", "-norc", "-pdflua", tex_path],
    stdin=subprocess.PIPE,
    check=True,
)
latex_process = subprocess.run(
    ["latexmk", "-norc", "-c", tex_path]
)
print(f"generated {pdf_path}")
for f in tmpdir.iterdir():
    f.unlink()
tmpdir.rmdir()
