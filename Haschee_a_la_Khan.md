# Haschee á la Khân

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *3* Zwiebeln, gewürfelt
- *1 Packung* Pilze, in Scheiben
- *2* Zehen Knoblauch, klein
- *400 g* Hackfleisch, hackig
- *1 Dose* Kokosmilch (halbe Dose ist besser, ist sonst recht kokoslastig)
- *400 g* Reis
- Salz
- Pfeffer
- Muskat
- Koriander
- Stärke
- Ras El Hanout (LIDL Gewürzmischung aus Kurkuma, Koriander, Chili, Fenchel, Salz, Knoblauch, Cumin, Bockshornklee, weißer Pfeffer, Ingwer)

---

_"Wenn man gut Hunger hat, wird man davon gut satt"_ -- zufriedener Esser

## 000 (königsdisziplin, aber optional) Reis ein Tag vorher einlegen

Reis über Nacht in Wasser einlegen, damit Stärke ausgetrieben wird und der Reis vorquillt.  
Falls nicht gemacht: den Reis _jetzt_ einlegen wo noch nichts geschnitten ist.

## 010 Zwiebel-Basis

Zwiebeln in Wok anbraten. Salz, Pfeffer und eine größere Menge Öl hinzufügen.
Wenn zufriedenstellender Zwiebel-Geschmack erreicht, goto 20

## 020 Pilze rein + Reis kochen

Pilze rein.  
Koriander (circa 1 TL) und Muskatnuss rein (nur ein Hauch)  
LIDL-Gewürzmischung (in diesem Fall: _Ras El Hanout_) hinzu - falls nicht vorhanden ist auch egal.  
Wenn zufriedenstellender Zwiebel-mit-Pilz-Geschmack erreicht, goto 30

### Reis starten

Jetzt beginnen, den Reis nach Packungsanleitung zu kochen.

## 030 Hackfleisch rein

Hackfleisch rein. Gut anbraten lassen.  
Kokosmilch rein.  
Das aus den Pilzen und dem Fleisch austretende Wasser wird durch Zugabe von *1 TL* Stärke gebunden.  
Kosten und überprüfen ob ein sinnvolles Lebensmittel dabei rum kam.

## 040 Intensität überprüfen

Das Pilz-Hackfleisch-Gemisch sollte einen guten, intensiven Geschmack haben.  
Falls Nein: Salz großzügig hinzugeben, aufkochen, kosten, GOTO 040.  
Falls Ja: Goto 050.

## 050 Servieren

Kann nach belieben serviert werden. Zum Beispiel zwei Schichten Reis mit einer Schicht Pilz-Hackfleisch-Gemisch geben.

---

Dazu passt halbtrockener Wein (in diesem Fall: 2019 Landwein, Reh Kellermann GmbH).
