# Chinesisch geschmorter Schweinebauch

毛氏紅燒肉, Pinyin: Máo shì hóngshāoròu

Geschmorter Schweinebauch nach Mao Zedong.

Für eine einfachere Variante kann man mit [chinesischem Fünf-Gewürze-Pulver](https://de.wikipedia.org/wiki/F%C3%BCnf-Gew%C3%BCrze-Pulver) würzen.

*chinesisch*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1 kg* Schweinebauch (alternativ fettiges Rindfleisch)
- *1,5 EL* Erdnussöl
- *1 EL* Zucker
- *0,33 L* Reiswein
- *200 ml* Sojasoße
- *1 Bund* Frühlingszwiebeln
- *400 g* Reis
- frische Chilischoten, je nach Geschmack, z.B. 1 Habanero
- *5 cm* Ingwer
- *2 Stangen* Zimt
- *5* Sternanis

---

## Fleisch blanchieren

Das Fleisch in 2cm dicke Scheiben schneiden.  
Wasser zum Kochen bringen und das Fleisch 4 Minuten darin kochen.  
Das Wasser beiseite stellen.  
Das Fleisch dann in ca. 2cm×2cm×2cm große Stücke schneiden.  

## Anbraten mit Karamellgeschmack

In einer Pfanne das Erdnussöl erhitzen.  
Den Zucker darin karamellisieren.  
Das Fleisch hinzugeben und 30 Sekunden unter Rühren braten.  
Mit Reiswein und Sojasoße ablöschen.  
Mit dem Wasser vom Fleisch auffüllen, bis das Fleisch bedeckt ist. 

Chili klein hacken.  
Ingwer schälen und klein hacken.  
Chili, Ingwer, Zimtstangen und Sternanis zum Fleisch geben.  

## Lange köcheln lassen

Mindestens 90 Minuten mit schwacher Hitze köcheln lassen.

In der Zwischenzeit die Frühlingszwiebeln waschen und klein hacken.

## Danach

Die Frühlingszwiebeln ins Fleisch geben.
Danach die Soße vom Fleisch unter starker Hitze reduzieren, bis sie zähflüssig ist. Das dauert ca. 30 Minuten.    
Mit Sojasoße, Salz und braunem Zucker abschmecken.

Den Reis kochen, damit der mit dem Fleisch fertig ist.

---

Quelle: https://www.chefkoch.de/rezepte/2526871396171950/Mao-Shi-Hong-Shao-Rou.html
