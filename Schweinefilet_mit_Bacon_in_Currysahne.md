# Schweinefilet mit Bacon in Currysahne

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1 kg* Kartoffeln
- *500 g* Schweinefilet
- *1,5 EL* Senf
- *100 g* Bacon
- *20 g* Petersilie, frisch oder tiefgefroren
- *250 ml* Sahne
- *250 g* Crème fraîche
- *20 g* Tomatenmark
- Salz
- Pfeffer
- Olivenöl
- Currypulver
- Cayennepfeffer
- Zucker

---

Den Backofen auf 200 °C (Ober-/ Unterhitze, empfohlen) oder 180 °C (Umluft) vorheizen.
Kartoffeln waschen und in Spalten schneiden.  
Auf eine Hälfte eines Backblechs geben, so dass die Auflaufform auf die andere Hälfte passt.  
Salzen, Pfeffern und mit Olivenöl beträufeln.  
25 Minuten backen.

Schweinefilet in ca 2 cm dicke Streifen (Medallions) schneiden.
Mit Salz und Pfeffer würzen und mit Senf bestreichen.  
Jedes Stück in 2 Richtungen mit Bacon umwickeln und in eine Auflaufform legen.

Petersilie waschen, trocken schütteln und fein hacken.  
In einer Schüssel Sahne, Crème fraîche, Curry, Tomatenmark und Petersilie verrühren.  
Mit Cayennepfeffer, Salz und Zucker abschmecken.  
Über das Fleisch geben.  
Die Auflaufform zu den Kartoffeln in den Ofen stellen.  
20 bis 15 Minuten garen.

---

Quelle: https://emmikochteinfach.de/schweinefilet-mit-bacon-in-currysahne/

