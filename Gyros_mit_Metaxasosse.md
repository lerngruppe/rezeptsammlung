# Gyros mit Metaxasoße

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1200 g* Gyros
- *2* Zwiebeln
- *200 g* Paprika
- *100 g* Sahne-Schmelzkäse
- *200 ml* Weißwein
- *200 ml* Gemüsebrühe
- *400 ml* Sahne
- *100 g* Tomatenmark
- *250 g* Nudeln
- *350 ml* Metaxa
- *250 g* Raspelkäse
- Olivenöl
- Salz
- Pfeffer
- Paprikapulver

---

Zwiebeln und Paprika klein schneiden.

Gyros in einer Pfanne scharf anbraten.  
Das Fleisch in eine Auflaufform füllen, den Sud in der Pfanne lassen.  
Zwiebeln und Paprika im Fleischsud anbraten und weich kochen, dabei eventuell etwas Olivenöl dazugeben.

Mit Weißwein und Brühe ablöschen.  
Den Schmelzkäse dazugeben.  
Die Sahne und das Tomatenmark einrühren.  
Mit Salz, Pfeffer und Paprikapulver abschmecken.  
Circa 30 Minuten einkochen.

Nudeln kochen und in der Auflaufform verteilen.  
Nach und nach Metaxa in die Pfanne geben.

Die Soße über das Fleisch gießen und mit Raspelkäse bedecken.

Im Backofen bei 180°C backen, bis der Käse goldbraun ist.

---

Quellen:

- https://www.chefkoch.de/rezepte/138071059660016/Gyros-ueberbacken-mit-Metaxa-Sahne-Sauce.html
- https://www.chefkoch.de/rezepte/649311166264932/Metaxasauce-a-la-Boris.html
