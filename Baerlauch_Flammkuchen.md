
# Bärlauch-Flammkuchen

**2 Bleche (3 [Lerngruppz](README.md#lerngruppz))**

---

## Teig
- *250 g* Dinkelmehl Typ 630
- *2 EL* Olivenöl
- *1 TL* Salz

## Creme
- *150 g* Crème fraîche
- etwas Milch
- *1 Spritzer* Zitronensaft
- Salz
- Pfeffer

## Belag
- *50 g* Bärlauch
- *280 g* getrocknete Tomaten in Öl
- *250 g* Feta

---

Mehl, Olivenöl, Salz mit 130 ml Wasser vermischen und Mit Knethaken zu einem Teig verarbeiten.  
In 2 Teile teilen und je auf Größe eines Backblechs auf einer bemehlten Arbeitsfläche ausrollen.

Crème fraîche mit Salz und Pfeffer würzen und mit Zitronensaft und etwas Milch glatt rühren.  
Die Creme auf dem Teig verteilen.

Den Backofen auf 230°C Ober-/Unterhitze vorheizen

Bärlauch waschen.  
Tomaten, Feta und Bärlauch klein schneiden.  

Die Bleche auf der obersten und untersten Schiene 10 Minuten backen.  
Die Bleche tauschen und nochmal 10 Minutne backen.

---

Quelle: https://www.tulpentag.de/flammkuchen-mit-baerlauch-getrockneten-tomaten-und-feta/
