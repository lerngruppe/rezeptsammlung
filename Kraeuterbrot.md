# Kräuterbrot

*zum Grillen*

**4 [Lerngruppz](README.md#lerngruppz)**

---

## Teig
- *500 g* weißes Mehl (Weizen oder Dinkel)
- *100 ml* Milch (oder vegane Alternativen)
- *100 ml* lauwarmes Wasser
- *1* Ei
- *1 EL* Butter
- *1 TL* Zucker
- *1,5 TL* Salz
- *1 Packung* Trockenhefe

## Geschmack
- *150 g* Kräuterbutter, Zimmertemperatur
- *100 g* Raspelkäse

## Zum Bestreichen
- *1* Ei

---

Den Teig mit Knethaken zusammenrühren und 45 Minuten gehen lassen.  
Auf 40x40 cm ausrollen.  
Mit der Kräuterbutter bestreichen.  
Den Käse darauf verstreuen.

Das Ei verquirlen.

Von links und rechts jeweils ca. 7 cm breite Streifen zu 1/3 einschneiden und in die Mitte falten.  
In eine mit Backpapier ausgelegte Kastenform legen.  
Mit dem Ei bestreichen.  
Den Backofen auf 180 °C Ober-/Unterhitze vorheizen.  
15 Minuten gehen lassen. 

45 bis 60 Minuten backen.
