# Grüner Salat mit Mandarinendressing

Tolle Beilage.

*einfach, beilage*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1 Kopf* Eisbergsalat
- *1/2* Zitronen, Saft davpn
- *200 ml* Sahne
- *175 g* Mandarinen aus der Dose
- Vanillezucker

---

Zitronensaft und Sahne verrühren und mit Vanillezucker abschmecken.

Den Salat in mundgerechte Stücke zupfen.  
Die Mandarinen abgießen und auf den Salat geben.

Die Soße über den Salat gießen.  
Leicht durchmengen.

---

Quelle: Mama von Jendrik
