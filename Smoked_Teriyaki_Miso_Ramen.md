# Smoked Teriyaki Miso Ramen

**6 [Lerngruppz](README.md#lerngruppz)**

---

- *4* Orangen mit essbarer Schale
- *25 g* Noriblätter
- *2* Chilischoten
- *60 g* Ingwer
- *9 Zehen* Knoblauch
- *750 ml* Rinder- oder Hühnerbrühe
- *1,2 L* Wasser
- *9 EL* Austernsoße
- *9 EL* Misopaste
- *18 EL* Sojasoße
- *9 EL* Honig
- *3 EL* Reis- oder Apfelessig
- *2 Bund* Frühlingszwiebeln
- *175 g* Mungobohnen- oder Sojasprossen
- *175 g* Bambusstreifen
- *1* große Karotte
- *250 g* Shiitake oder Champignons
- *1,2 kg* Fleisch (z.B. Huhn, Pute, Rind, Garnelen, ...)
- *750 g* asiatische Nudeln

---

Die Orangen waschen, die Schale abreiben und den Saft auspressen.

Noriblätter in 2cm × 2cm große Stücke schneiden.  
Chilischoten waschen und klein schneiden.  
Ingwer schälen und klein hacken.  
Knoblauch schälen und klein hacken.  
Brühe mit Wasser zum Kochen bringen.  
Noriblätter, Chili, Austernsoße, Misopaste, die Hälfte der Sojasoße, die Hälfte des Orangensafts, 2/3 des Ingwers, 2/3 des Knoblauchs und 2/3 des Honigs einrühren.  
45 Minuten köcheln lassen.
Den Essig dazugeben.

Frühlingszwiebeln, Pilze und Karotte waschen.  
Alles in kleine Stücke schneiden.  
Zu der Brühe geben.

Für die Marinade in einer Pfanne Orangenschale, die Hälfte der Sojasoße, die Hälfte des Orangensafts, 1/3 des Honigs, 1/3 des Ingwers, 2/3 des Knoblauchs einkochen.  
Das Fleisch in kleine Stücke schneiden.  
In der Marinade vorsichtig braten.

Die Nudeln nach Packungsanleitung kochen.  
Dabei kein Salz verwenden.

---
