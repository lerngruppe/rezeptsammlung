# KKK: Kichererbsen-Kurkuma-Kokos-Eintopf

"äußerst bekömmlich"

"verleiht ein wohliges Bauchgefühl"

**3 [Lerngruppz](README.md#lerngruppz)**

---

- *4* Zwiebeln
- *3 Zehen* Knoblauch
- *2-3 cm* Ingwer
- *480 g* Kichererbsen (Dose oder Glas)
- *2 Dosen* Kokosmilch
- *400 ml* Gemüsebrühe
- *1* Fenchel
- *4* Möhren
- *1* Paprika
- *480 g* geschälte Tomaten (Dose)
- *1 bis 2 EL* Kurkuma
- *1 TL* Kreuzkümmel
- *1 TL* Koriander
- *2* Chilischoten
- Öl zum Braten

---

Zwiebeln, Knoblauch und Ingwer schälen und klein hacken.  
In einer Pfanne mit Öl anbraten.  
Kurkuma, Kreuzkümmel, Koriander und Chili dazugeben.

Die Kichererbsen dazugeben und 10 Minuten mitbraten.

Mit Kokosmilch und Gemüsebrühe ablöschen.

Das restliche Gemüse klein schneiden und in den Topf geben.  
Ca. 30 Minuten köcheln lassen, bis das Gemüse gar ist.