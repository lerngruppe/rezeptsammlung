# Shepherd's Pie

Gebackener Kartoffelbrei

*englisch*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *1200 g* Kartoffeln
- *8* Zwiebeln
- *800 g* Möhren
- *500 g* Hackfleisch
- *120 g* Tomatenmark
- *2 EL* HP-Sauce
- Thymian
- *8 EL* Mehl
- *1 L* Rinderbrühe
- *280 g* Erbsen, tiefgekühlt oder Dose
- *4 EL* Butter
- *120 ml* Milch
- *300 g* Cheddar
- *4 EL* Senf
- Salz
- Pfeffer
- Öl zum Braten

---

Kartoffeln schälen, waschen, in Stücke schneiden und in Salzwasser gar kochen.

Zwiebeln schälen und klein schneiden.  
Möhren waschen und klein schneiden.  
Beides in einer Pfanne in Öl anbraten.  
Mit Tomatenmark, HP-Sauce und Thymian würzen.

Rinderbrühe und Erbsen hinzufügen.

Kartoffeln mit Butter, Milch, Cheddar, Senf, Salz und Pfeffer stampfen.

Den Backofen auf 180° C vorheizen.

Hackfleischmasse in eine Auflaufform geben und mit dem Kartoffelbrei bedecken.

Für 30 Minuten backen.

---

Quelle: KptnCook
