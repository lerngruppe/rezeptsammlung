# Süßkartoffeln mit Avocado-Creme

*vegetarisch*

**2 [Lerngruppz](README.md#lerngruppz)**

--- 

- *6* große Süßkartoffeln
- *3* Avocados, reif
- *150 g* Schmand oder Frischkäse
- *2* Zwiebeln
- *2* Knoblauchzehen
- *1* Tomate
- *2 EL* Limetten- oder Zitronensaft
- etwas Petersilie
- Sonnenblumen- oder Rapsöl
- Salz
- Pfeffer
- Chiliflocken

---

Süßkartoffeln gründlich waschen und nebeneinander auf ein Backblech legen.  
Ofen auf 200 °C Ober-/Unterhitze vorheizen.  
Öl mit Salz und Pfeffer vermischen und die Süßkartoffeln rundherum damit bepinseln.

Im Ofen je nach Größe ca. 45 - 50 Minuten garen, alle 15 min wenden.

Zwiebel und Knoblauch fein würfeln.  
In Öl anbraten.

Tomate ganz fein würfeln,
mit den Zwiebeln kurz anbraten.

Die Avocados halbieren, entkernen und das Fruchtfleisch herauslöffeln.  
Dieses mit Schmand bzw. Frischkäse, dem Pfanneninhalt, 3/4 der Petersilie und Limetten- bzw. Zitronensaft vermischen.  
Mit Salz, Pfeffer und Chiliflocken abschmecken.

Die gegarten Süßkartoffeln der Länge nach einschneiden,  
mit der Avocado-Tomaten-Creme füllen.

Mit Chiliflocken und Petersilie bestreuen und servieren.

---

Quelle: https://www.chefkoch.de/rezepte/2725361425059942/Gebackene-Suesskartoffeln-mit-Avocado-Paprika-Creme.html