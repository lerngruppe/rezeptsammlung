# Grillkäse

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *2 Stück* Schafskäse
- *2* Zwiebeln
- Paprikapulver
- Sonnenblumenöl.

---

Zwiebeln in Halbringe hacken.

Alufolie in der Mitte mit Öl bedecken.  
Käse auf Alufolie legen.  
Mit den Zwiebeln umgeben.  
Mit Paprikapulver würzen.  

Die Alufolien zu Beuteln falten.

Circa 10 Minuten Grillen.