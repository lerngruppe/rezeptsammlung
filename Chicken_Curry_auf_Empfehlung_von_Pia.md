# Chicken-Curry auf Empfehlung von Pia

*simple, quick*

**4 Portionen**

---

## Curry
- *600 g* Hähnchenbrustfilet
- *2* Zwiebeln, geschält
- *2* Paprika
- *1* Lauch
- *2* kleine Möhren
- *400 ml* Kokosmilch, ungesüßt
- *150 g* Joghurt
- *2 EL* Currypulver, leicht gehäuft
- *5 EL* Tomatenmark
- *2* Knoblauchzehen
- Salz
- Pfeffer
- Paprikapulver
## Beilagen
- optional: Basmati Reis

---

# Hinweis zur Beilage
Der Reis kann zeitgleich begonnen werden, es tut diesem geschmacklich gut bei geschlossenem Deckel zu ziehen.  
Marcel sagt: Reis vorher waschen.

# Zerkleinern
Zwiebeln fein hacken.  
Lauch in feine Ringe schneiden.  
Paprika würfeln  
Möhren schälen, längs halbieren und in dünne Scheiben schneiden
Knoblauchzehen schälen.

# Nicht-Fleisch kochen
Zwiebeln glasig dünsten in etwas Öl in einem Wok/Pfanne.  
danach das übrige Gemüse hinzugeben und kurz anbraten.  
Den Knoblauch pressen und zum Gemüse geben.  
Alles mit Currypulver bestäuben und mit Joghurt und Kokosmilch ablöschen.  
Das Tomatenmark hinzugeben und gut durchrühren.  
**Alles ca. 20 Minuten lang bei geschlossenem Deckel leise köcheln lassen, bis das Gemüse gar ist.**

# Fleisch kochen
Hähnchenbrustfilets waschen, trocken tupfen, in dünne Streifen schneiden.  
Mit Salz und Pfeffer würzen.  
Dies etwas ruhen zu lassen in dieser Marinade schien dem Fleisch bisher sehr gut getan zu haben.  
In einer Pfanne in etwas Öl scharf anbraten, bis sie goldbraun sind.

# Kombinieren und Abschmecken
5 Minuten vor Ende das Fleisch hinzugeben und erwärmen.  
Mit Salz, Pfeffer, Currypulver und Paprikapulver abschmecken.

---

Reihenfolge Fleisch/Nicht-Fleisch wurde gedreht weil ergab im Originalrezept keinen Sinn.  
Quelle: https://www.chefkoch.de/rezepte/1582261265704130/Chicken-Curry.html
