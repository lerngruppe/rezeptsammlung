# Rezeptsammlung der Lerngruppe

Im RecipeMD Format angesammelt und gepflegt über die Jahre.

[**Download as PDF**](https://gitli.stratum0.org/lerngruppe/rezeptsammlung/-/jobs/artifacts/master/raw/Rezeptsammlung.pdf?job=make_pdf)

Kochrezepte, welche ausm Internet zusammengeklaut wurden.
Hauptverzeichnis listet nur Dinge auf welche bereits gekocht und für gut befunden wurden (unkreative "habe Hunger und möchte etwas kochen"-Liste).
Im Ordner "mal-ausprobieren" befinden sich Rezepte, welche noch nicht probiert wurden, welche mir jedoch _womöglich_ schmekcen und somit ausprobiert werden sollten.

Siehe https://recipemd.org für RecipeMD Syntax


## Lerngruppz

Lerngruppz ist eine Einheit welche das gängige _Portion_ in dieser Repo versucht abzulösen.  
1 Lerngruppz bezeichnet die Menge an Nahrung, welche erforderlich ist, ein Lerngruppen-Mitglied (männlich, 75 kg, 20-30) satt zu bekommen.  
Häufig ist eine _"Portion"_ wie diese auf Chefkoch und anderen Website zu finden um Faktor 2 bis 3 zu wenig.
