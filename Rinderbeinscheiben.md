
# Rinderbeinscheiben

*schmoren*

**4 [Lerngruppz](README.md#lerngruppz)**

---

- *4* Rinderbeinscheiben
- *8* Zwiebeln
- *4* Zehen Knoblauch
- *8* Möhren
- *10* Tomaten
- *2 EL* Tomatenmark
- *400 ml* Rotwein
- *400 ml* Gemüsebrühe
- Salz
- Pfeffer
- Mehl
- Öl zum Braten
- Thymian
- *4* Lorbeerblätter

---

Von den Beinscheiben den Fettrand an 4 Stellen einschneiden.  
Die Beinscheiben salzen, pfeffern und mehlieren.  
Das überschüssige Mehl abklopfen.

Eine große Pfanne mit Öl erhitzen.  
Die Beinscheiben von beiden Seiten kurz anbraten und beiseite legen.  

Zwiebeln würfeln, Knoblauch klein hacken.  
Beides in der Pfanne anbraten.

Möhren und Tomaten waschen, klein schneiden und in die Pfanne geben.  
Mit Thymian, Salz und Pfeffer, Lorbeerblättern und Tomatenmark würzen.

Mit dem Rotwein und der Gemüsebrühe ablöschen.  
Die Beinscheiben mit ausgelaufenem Saft auf das Gemüse legen.  
Auf kleinster Stufe mit Deckel mindestens 2:30 h schmoren.

---

Quelle: https://www.chefkoch.de/rezepte/2966561449118546/Rinderbeinscheiben-geschmort.html
