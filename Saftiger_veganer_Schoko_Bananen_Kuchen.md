# Saftiger, veganer Schoko-Bananen-Kuchen

Vegan, einfach, mit Rohrzucker.  

*cake, vegan, halal*

**1 Kuchen**

--- 

- *225 g* Weizenmehl (Typ 405)
- *3 TL (15 g)* Backpulver
- *1 EL (8 g)* Vanillezucker
- *60 g* Rohrzucker
- *4 EL, gehäuft (60 g)* Kakao
- *70 g* Mandeln, gerieben
- *100 ml* Sonnenblumenöl
- *250 ml* Milch oder Milchersatz (z. B. [Haferdrink](https://www.dm.de/dmbio-barista-hafer-drink-mit-soja-p4058172455674.html))
- *5 EL, gehäuft* Schokoladencreme ([Zartbittercreme](https://www.dm.de/dmbio-schokoladen-aufstrich-zartbitter-creme-p4058172451294.html))
- *3* Bananen, reife

---

### trockene Zutaten vermischen

Mehl, Backpulver, Zucker, Vanillezucker, Kakao und Mandeln miteinander mischen.

Der Backofen sollte auf 175° Ober-Unterhitze vorgeheizt werden.

### in Topf Grundmasse erhitzen

In einem Topf das Sonnenblumenöl, Pflanzendrink & Zartbittercreme erhitzen.  
Nebenbei umrühren so dass die Zartbittercreme schmilzt und alles zu einer homogenen Masse wird.  
Die Bananen zerdrücken.  

### Mischen & in Form geben

Das Öl-Drink-Schokoladen-Gemisch unter die trockenen Zutaten mischen,  
zu einem Teig verrühren und die Bananen hinzugeben.
Den Teig in die vorgefettete Form geben.

### Backen

ca. eine Stunde im Ofen backen.

Mit der Stäbchenprobe könnt ihr überprüfen, ob er wirklich schon fertig ist: sollte Teig kleben bleiben, lasst ihn noch ein wenig länger im Ofen. Also z. B. in der 28 cm Form mit Loch von saxnot hat es nur 35 min gedauert.
Wenn er fertig ist, nehmt ihn raus und lasst ihn auf einem Gitter auskühlen.

---

Wurde mit Vollkorn-Dinkelmehl probiert, allerdings war der Kuchen dann ziemlich trocken.

Quelle: https://www.chefkoch.de/rezepte/3015031454270014/Saftiger-veganer-Schoko-Bananen-Kuchen.html
