# Thunfisch mit Sesammarinade

It's fish. Your tagline here!

**2 [Lerngruppz](README.md#lerngruppz)**

---

- *1.4 kg* Kartoffeln
- *4* Thunfischfilets a 125g, aufgetaut
- *2 EL* Sesam
- *1 TL* Chiliflocken
- *1 TL* Paprikaflocken oder Paprikapulver
- *1 TL* Salz
- *3* rote Zwiebeln
- *1* Knoblauchzehe
- *300 g* Weißwein
- *2 TL* Kräuterfrischkäse

---

## Rösti

Kartoffeln kleinhobeln und in Pfanne zu Rösti verwandeln

## Fisch

### Abtrocknen

Fisch auftauen & mit Küchenkrepp abtrocknen.

### $stuff hacken

Zwiebel und Knoblauch kleinschneiden,  
In einer großen Pfanne in Öl anbraten.  
Mit Weißwein ablöschen.  
Frischkäse hinzugeben und kurz kochen lassen.

### Marinade

Sesam, Chiliflocken und Paprikaflocken/-pulver und Salz auf einem Teller vermischen.  
Den Thunfisch darin wenden.

## Alles kombinieren

Die Soße aus der Pfanne gießen.  
Fisch hineingeben und bei geringer Hitze ca. 3 Minuten braten.
Evtl dabei Öl ergänzen.
